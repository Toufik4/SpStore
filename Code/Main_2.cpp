#ifdef DB_ENGINE_ODBC
#include <SpStore/Interface/SpODBC.h>
#include <iostream>

int main_2(int argc, char **argv)
{   
    SpODBC::Connection my_Connection;
    SpODBC::Command my_Command;

	const char* sServer[] = { "MySQL_Native" , "SQLEXPRESS" };
	const char* sRq = "Select id, name, contact from userinfotbl";
	const char* sRq0 = "select * from Table1";
	const char* sRq2 = "INSERT INTO Table1 (id, name, contact) VALUES(\'6\', \'SpODBC_6\', \'squel6@tiny.com\')";
	const char* sRq3 = "DELETE FROM Table1 WHERE id=\'5\'";
    // Create a Connection with an ODBC Data Source
    if (!my_Connection.Connect("SQL_SERVER_Toufik", "toufik", "123abcABC@"))
    {
        std::cout << "Cannot connect to the Data Source" << std::endl
            << my_Connection.LastError();
        return 1;
    }

    // Execute a direct query
    if (!my_Command.ExecuteDirect(my_Connection, sRq2))
    {
        std::cout << "Cannot execute query!" << std::endl
            << my_Command.LastError();
        return 2;
    }

    // Get results from Command
    while(my_Command.FetchNext())
    {
        // print all fields for each row
        for(int i = 1;i <= my_Command.count_columns(); i ++)
            std::cout << my_Command.field(i).as_string() << "\t";

        std::cout << std::endl;
    }
	 // Execute a direct query
    if (!my_Command.ExecuteDirect(my_Connection, sRq2))
    {
        std::cout << "Cannot execute query!" << std::endl
            << my_Command.LastError();
        return 2;
    }
	exit(0);
    //return 0;
}

#endif // DB_ENGINE_ODBC