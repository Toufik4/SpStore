//*****************************************************************************
//
//  Copyright (c) Matra Transport International 2001
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpDB_DBLib.cpp
//  Auteur   : Jean BRETAUDEAU
//  Cr�ation : jeudi 13 septembre 2001
//  Contenu  : D�finition de la classe SpDB_DBLib
//
//*****************************************************************************
//  Modifications : 21335_OT_1 : A. Kolybine : Gestion des deconnexions SQL Server
//*************************************************************************************************
//  Modifications : 24/02/2004 MHO uniformisation message d'erreur avec traces 
//*************************************************************************************************
//#define DB_ENGINE_DBLIB
#ifdef DB_ENGINE_DBLIB

#include <SpStore/Interface/SpDB_DBLib.h>
#include <SpCmn/Interface/SpCmnAssertion.h>
#include <SpCmn/Interface/SpCmnApplicationPolicies.h>
#include <SpCmn/Interface/SpCmnParamAppli.h>
#include <XtWin/Interface/XtWinThreading.h>

namespace
{
	//## timeout for dbopen
	int connection_timeout_in_seconds = 1;

	//## Query timeout - 0 signifie no timeout (attente infinie)
	int query_timeout_in_seconds = 0;
	
	//## the tabular data stream (TDS) packet size
	int tds_packet_size = 0;

	//## retry delay and nb for dbopen 
	int connection_retry_delay_in_milliseconds = 250;
	int connection_retry_number = 3;

	//## retry delay and nb for dbuse 
	int dbuse_retry_delay_in_milliseconds = 1000;
	int dbuse_retry_number = 3;

	bool authentication_windows = false;

	struct SpCmnSQLPropertiesInitialisation
	{
		static void SetApplicationProperties(const SpCmnParamAppli& properties)
		{
			connection_timeout_in_seconds = properties.getIntegerValue("archive.connection.timeout", connection_timeout_in_seconds);
			connection_retry_delay_in_milliseconds = properties.getIntegerValue("archive.connection.retry.period", connection_retry_delay_in_milliseconds);
			connection_retry_number = properties.getIntegerValue("archive.connection.retry.number", connection_retry_number);
			query_timeout_in_seconds = properties.getIntegerValue("archive.query.timeout", query_timeout_in_seconds);
			tds_packet_size = properties.getIntegerValue("archive.tds.packet_size", tds_packet_size);
			dbuse_retry_delay_in_milliseconds = properties.getIntegerValue("archive.dbuse.retry.period", dbuse_retry_delay_in_milliseconds);
			dbuse_retry_number = properties.getIntegerValue("archive.dbuse.retry.number", dbuse_retry_number);
			authentication_windows = properties.getBooleanValue("archive.authentication.windows", authentication_windows);
		}
	};
	SPCMN_DEF_APPLICATIONPOLICY(SpCmnSQLPropertiesInitialisation);
}

/*
//FIXME creer un fichier d'include
// D�sactivation des traces si SPSTORE_LOG_DEBUG n'est pas d�finie
#ifndef SPSQL_LOG_DEBUG
#  define SPSQL_NDEBUG
#endif

// traces 'spocc.sql'
#ifndef SPSQL_NDEBUG
#  define SPSQL_DEBUG(msg)				XTWIN_LOGDEBUG(spocc.sql, msg)
#else
#  define SPSQL_DEBUG(msg)
#endif

#define SPSQL_TRACE(msg)				XTWIN_LOGTRACE(spocc.sql, msg)
#define SPSQL_INFO(msg)					XTWIN_LOGINFO(spocc.sql, msg)
#define SPSQL_NOTE(msg)					XTWIN_LOGNOTE(spocc.sql, msg)
#define SPSQL_WARNING(msg)				XTWIN_LOGWARNING(spocc.sql, msg)
#define SPSQL_ERROR(msg)				XTWIN_LOGERROR(spocc.sql, msg)


// traces conditionnelles 'spocc.sql'
#ifndef SPSQL_NDEBUG
#  define SPSQL_DEBUGIF(pre, msg)		XTWIN_LOGDEBUGIF(spocc.sql, pre, msg)
#else
#  define SPSQL_DEBUGIF(pre, msg)
#endif

#define SPSQL_TRACEIF(pre, msg)			XTWIN_LOGTRACEIF(spocc.sql, pre, msg)
#define SPSQL_INFOIF(pre, msg)			XTWIN_LOGINFOIF(spocc.sql, pre, msg)
#define SPSQL_NOTEIF(pre, msg)			XTWIN_LOGNOTEIF(spocc.sql, pre, msg)
#define SPSQL_WARNINGIF(pre, msg)		XTWIN_LOGWARNINGIF(spocc.sql, pre, msg)
#define SPSQL_ERRORIF(pre, msg)			XTWIN_LOGERRORIF(spocc.sql, pre, msg)
#define SPSQL_FATALERRORIF(pre, msg)	XTWIN_LOGFATALERRORIF(spocc.sql, pre, msg)

*/


int msg_handler_SQL (PDBPROCESS dbproc, DBINT msgno, INT msgstate,
    INT severity, LPCSTR msgtext, LPCSTR server,
    LPCSTR procedure, DBUSMALLINT line)
{
	if (severity <= 0)	// severity 0 non document�e !!!
	{
		// ne pas faire de traces, elles sont inint�ressantes et prennent du temps
	}
	else if (severity <= EXINFO)
	{
		SPSQL_NOTE("SQL Server message : no: " << msgno
			<< " text: " <<  msgtext);
	}
	else
	{
		SPSQL_ERROR("SQL Server error : no: " << msgno
			<< " severity: " << severity << " text: " <<  msgtext);
	}

	return 0;
}


bool SpDB_DBLib::_is_initialised(false);


SpDB_DBLib::SpDB_DBLib()
	: _dbproc(0)
	, _old_dbproc(0)
	, _transaction_en_cours(false)
{
}

SpDB_DBLib::~SpDB_DBLib()
{
}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : initConnect
//.--------------- 
//. 
//.Description :	m�thode d'initialisation de la connection au  
//.-----------		serveur SQL  
//.					loginName est le nom de login au niveau de la base SQL 
//.					avec son mot de passe loginPasswd
//.					serverName, le nom du serveur SQL et basename la 
//.					base sur laquelle on veut faire les requ�tes. 
//.						
//.Returned Value : si des erreurs existent, elles sont d�rout�es sur
//.--------------	les proc�dures de traitements des erreurs msg_handler
//.					et err_handler
//. 
//.================================================================= 
void SpDB_DBLib::initConnect(const char* loginName, const char* loginPasswd,
	const char* processName, const char* serverName, const char* baseName, const int timeOut)
{
    // close the previous connection if necessary
    if (_old_dbproc)
    {
		SPSQL_INFO("Close SQL Server connection (old_dbproc pointer value : " << _old_dbproc << ")");
		dbclose(_old_dbproc);
        _old_dbproc = 0;
    }

	// error mgnt handle functions
	dberrhandle((DBERRHANDLE_PROC)err_handler);
    dbmsghandle(msg_handler_SQL);

	// initialisation  dblib  
	if (!_is_initialised) {
		LPCSTR ver = dbinit();
		
		if (ver) {
			_is_initialised = true;
			SPSQL_DEBUG(ver << " initialized");
		} else {
			SPSQL_ERROR("unable to initialize DBLIB");
			return;
		}
	}

	int ret; // SUCCEED OR FAIL

	//  Allocate and initialize the LOGINREC structure to be used
	//  to open a connection to SQL Server.
	LOGINREC* loginrec = dblogin();
	SPOCC_ASSERTION(loginrec, "database login allocation failed", return);
	
	ret = DBSETLAPP(loginrec, processName);	// Application name
	SPOCC_ASSERTION(ret, "setting application name failed", dbfreelogin(loginrec); return);
	
	ret = DBSETLUSER(loginrec, loginName);	// Login ID
	SPOCC_ASSERTION(ret, "setting login ID failed", dbfreelogin(loginrec); return);

	ret = DBSETLPWD(loginrec, loginPasswd);	// Password
	SPOCC_ASSERTION(ret, "setting password failed", dbfreelogin(loginrec); return);
	
	if (authentication_windows)
		DBSETLSECURE(loginrec);	// windows authentication using the current windows account, the previous login and password are not taken into account

	int timeoutvalue = connection_timeout_in_seconds > 1 ? connection_timeout_in_seconds : timeOut;
	// the timeout for dbopen below. If not set, DB-lib default would be 60 sec
	ret = DBSETLTIME(loginrec, timeoutvalue); 
	SPOCC_ASSERTION(ret, "setting login timeout failed", dbfreelogin(loginrec); return);
	
	if(tds_packet_size > 0)
	{
		ret = DBSETLPACKET (loginrec, tds_packet_size);
		SPOCC_ASSERTION(ret, "setting TDS packet size failed", dbfreelogin(loginrec); return);
	}
	
	// connection retry loop
	int nb_ret = 1;
	_dbproc = dbopen(loginrec, serverName);
	while (_dbproc == 0 && nb_ret < connection_retry_number)
	{
		SPSQL_ERROR("Error on dbopen");
		xtwin::sleep_thread(connection_retry_delay_in_milliseconds);
		_dbproc = dbopen(loginrec, serverName);
		nb_ret++;
	}
	dbfreelogin(loginrec);
	SPSQL_ERRORIF(_dbproc == 0, "failed to connect to SQL Server " << serverName);
	SPSQL_INFOIF(_dbproc, "Connected to SQL Server (dbproc pointer value : " << _dbproc << ")");
	
	if(query_timeout_in_seconds > 0)
	{
		ret = dbsettime(query_timeout_in_seconds);
		if(ret == FAIL)
		{
			SPSQL_ERROR("error while calling dbsettime");
		}
	}

	// attach 'this' pointer to the DBPROCESS, so that we'll be able to access it
	// in the error mgnt handle functions
	dbsetuserdata(_dbproc, this);

	// For information only.
	// For large data transfers, a packet size between 4096 and 8192 bytes is usually best. Any larger size can degrade performance.
	// Default size seemns to be 4096.
	UINT packet_size = dbgetpacket(_dbproc);
	SPSQL_INFO("tabular data stream (TDS) packet size currently in use " << packet_size);
  		
	// executes a Transact-SQL USE statement 
	ret = dbuse(_dbproc, baseName);
	SPSQL_DEBUG( "USE DATABASE " << ret);
	nb_ret = 1;
	while (ret == 0 && nb_ret < dbuse_retry_number)
	{
		SPSQL_ERROR("Error on USE database statement");
		xtwin::sleep_thread(dbuse_retry_delay_in_milliseconds);
		ret = dbuse(_dbproc, baseName);
		nb_ret++;
	}
	// force Disconnection if dbuse still fails
	if (ret == 0)
	{
		onDisconnection();
	} 
}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : closeConnect
//.--------------- 
//. 
//.Description :  deconnexion 
//.----------- 
//. 
//.=================================================================
void SpDB_DBLib::closeConnect()
{
	if (_dbproc)
	{
		SPSQL_INFO("Close SQL Server connection (dbproc pointer value : " << _dbproc << ")");
		dbclose(_dbproc);
		_dbproc = 0;
	}
}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : onDisconnection
//.--------------- 
//. 
//.Description :  fonction appel�e en cas de perte du serveur SQL
//.----------- 
//. 
//.=================================================================
void SpDB_DBLib::onDisconnection()
{
	SPSQL_ERROR( "SQL Server broken link" );
//	closeConnect();

	_transaction_en_cours = false;
    _old_dbproc = _dbproc;
	_dbproc = 0;

}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : begin_transaction
//.--------------- 
//. 
//.Description :  d�but de transaction pour les db librairies, obligatoire
//.-----------	  avant d'�crire dans une table SQL
//. 
//.					GARDEE POUR DES RAISONS DE COMPATIBILITE AVEC LA CLASSE
//.				    SINGLETON 
//.=================================================================
void SpDB_DBLib::begin_transaction()
{
	if (_dbproc)
	{
		_transaction_en_cours = true;
		dbcmd(_dbproc,"BEGIN TRANSACTION\n");
	}
}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : commit_transaction
//.--------------- 
//. 
//.Description :  fin de transaction pour les db librairies, obligatoire
//.-----------	  pour la prise en compte des �critures dans la base SQL
//. 
//.					GARDEE POUR DES RAISONS DE COMPATIBILITE AVEC LA CLASSE
//.				    SINGLETON 
//.=================================================================
void SpDB_DBLib::commit_transaction()
{
	if (_dbproc)
	{
		dbcmd(_dbproc,"COMMIT TRANSACTION\n");
		_transaction_en_cours = false;
	}
}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : executeQuery
//.--------------- 
//. 
//.Description :  ex�cution d'une commande pass�e en param�tre 
//.-----------	  
//.
//.Returned value : si tout est OK, DBPROCESS* est un pointeur vers 
//.--------------   la structure contenant les r�sultats de la requete 
//.					si la requ�te a �chou�, retour = NULL 
//.=================================================================
DBPROCESS*	SpDB_DBLib::executeQuery(const char* command) 
{
	if (_dbproc)
	{
	 	dbcmd(_dbproc, command); 
  		dbsqlexec(_dbproc);
	}
	return _dbproc;
}

//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : err_handler
//.--------------- 
//. 
//.Description :  fonction appell�e en cas d'erreur SQL Server
//.----------- 
//. 
//.Returned Value : (int) : vaut toujours INT_CANCEL
//.-------------- 
//. 
//.=================================================================
int SpDB_DBLib::err_handler(DBPROCESS *dbproc, int severity, int dberr, int oserr, char *dberrstr, char *oserrstr)
{
	// desactive le traitement d'erreur pour eviter une recusrssion infinie
	dberrhandle(NULL); 

	// recupere l'instance de SpDB_DBLib associ�e au DB PROCESS
	SpDB_DBLib* instance = static_cast<SpDB_DBLib*>(dbgetuserdata(dbproc));
	if (instance)
	{
		// traitement de l'erreur
		instance->errorSQL(dbproc, severity, dberr, oserr, dberrstr, oserrstr);
		
		if (dbdead(dbproc)) {			
			// cas particulier de la deconnexion
			instance->onDisconnection();
		}
	}
	else
	{
		// lors de l'�chec de la connexion (dbopen), le contexte n'est pas encore enregistr�
	 	SPSQL_ERRORIF(oserr != DBNOERR, "Operating-system error : " << oserrstr);
	 	SPSQL_ERROR("DB-Library error : " << dberrstr);
	}
	
	dberrhandle((DBERRHANDLE_PROC)err_handler);
	return INT_CANCEL;
}
/*
void SpDB_DBLib::errorSQL(DBPROCESS *dbproc, int severity, int dberr, int oserr, char *dberrstr, char *oserrstr)
{
	SPSTORE_ERROR("DB-LIBRARY ERREUR : " << dberrstr);

	if (oserr != DBNOERR)
	{
		SPSTORE_ERROR("Operating-system error: " << oserrstr );
	}
}
*/
//.=================================================================
//.Class : SpDB_DBLib
//.----- 
//. 
//.Member Function : errorSQL
//.--------------- 
//. 
//. 
//.Description :  gestion des erreurs remont�es par le serveur SQL 
//.----------- 
//.=================================================================
void SpDB_DBLib::errorSQL(DBPROCESS *dbproc, int severity, int dberr, int oserr, char *dberrstr, char *oserrstr)
{
 	SPSQL_ERROR("DB-Library error : severity: " << severity << " text: " << dberrstr);
 
	if (severity == EXCOMM && (oserr != DBNOERR || oserrstr))
		SPSQL_ERROR("Net-Lib error : no: " << oserr << " text: " << oserrstr) ;

	if (oserr != DBNOERR)
		SPSQL_ERROR("Operating-system error : " << oserrstr) ;
}

// Methodes translated from SpStoreDBManager Class
bool SpDB_DBLib::NextRow(t_Row& row)
{ 
    DBPROCESS* dbproc = getDbproc();

    if (dbproc) {
		row.bind();
		return dbnextrow(dbproc) != NO_MORE_ROWS;
	} else	{
		return false;
	}
}


void SpDB_DBLib::bind(t_Row& row)
{
	DBPROCESS* dbproc = getDbproc();
	t_RestoreRow* ptRow = dynamic_cast<t_RestoreRow*>(&row);

	if (ptRow != nullptr){
		dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)ptRow->getId());
		dbbind (dbproc,2,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)ptRow->getAttr());
		dbbind (dbproc,3,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)ptRow->getValue());
	}
	else{
	    t_IdRow* ptRow = dynamic_cast<t_IdRow*>(&row);
		if(ptRow != nullptr){
			dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)ptRow->getId());
		}
	}
}
/*
void SpDB_DBLib::bind1(const char* id)
{
	DBPROCESS* dbproc = getDbproc();
	dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)id);
}

void SpDB_DBLib::bind2(const char* id, const char* attr, const char* value)
{
	DBPROCESS* dbproc = getDbproc();
	dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)id);
	dbbind (dbproc,2,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)attr);
	dbbind (dbproc,3,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)value);
}
*/
void SpDB_DBLib::BeginTransaction()
{ 
	if (_dbproc) 
		dbcmd(_dbproc, "BEGIN TRANSACTION\n"); 
}

void SpDB_DBLib::CommitTransaction()
{ 
	if (_dbproc) 
		dbcmd(_dbproc, "COMMIT TRANSACTION\n");
}
/*
void SpDB_DBLib::AddQuery(const IlsString& req)
{ 
	if (_dbproc) 
		dbfcmd(_dbproc, "%s\n", req.value());	
}
*/
void SpDB_DBLib::AddQuery(const std::string& req)
{ 
	if (_dbproc) 
		dbfcmd(_dbproc, "%s\n", req.c_str());	
}

void SpDB_DBLib::ExecQuery()
{ 
	if (_dbproc) 
		dbsqlexec(_dbproc); 
}

void SpDB_DBLib::CancelQuery()
{ 
	if (_dbproc) 
		dbcancel(_dbproc); 
}

bool SpDB_DBLib::HasLastQuerySucced()
{ 
	return isGoodConnection() && dbresults(_dbproc) == SUCCEED; 
}

bool SpDB_DBLib::NoMoreResults()
{ 
	return isGoodConnection() && dbresults(_dbproc) == NO_MORE_RESULTS; 
}

bool SpDB_DBLib::Failed()
{ 
	return isGoodConnection() && dbresults(_dbproc) == FAIL; 
}

/*
void SpDB_DBLib::bindRegularResultColumns(SpDB_DBLib::t_IdRow& row)
{
	 dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)row.getId()); 
}

void SpDB_DBLib::bindRegularResultColumns(SpDB_DBLib::t_RestoreRow& row)
{

	dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)row.getId()); 
	dbbind (dbproc,2,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)getAttr());
	dbbind (dbproc,3,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)getValue());

}
*/

// BEGIN Row definitions
/*
//.=================================================================
SpDB_DBLib::t_IdRow::t_IdRow(SpDB_DBLib* data_base) :
_data_base(data_base),
_bound(false)
{
}

//.=================================================================
SpDB_DBLib::t_RestoreRow::t_RestoreRow(SpDB_DBLib* data_base) :
t_IdRow(data_base)
{
}

bool SpDB_DBLib::NextRow(SpDB_DBLib::t_Row& row)
{ 
    if (_dbproc) {
		row.bind();
		return dbnextrow(_dbproc) != NO_MORE_ROWS;
	} else	{
		return false;
	}
	
	// return _ptDBEngine->NextRow(row);
}

//.=================================================================
void SpDB_DBLib::t_IdRow::bind()
{
    if (!_bound && _data_base->getDbproc()) {
		bindRegularResultColumns();
		_bound = true;
	}
	
}

//.=================================================================
void SpDB_DBLib::t_IdRow::bindRegularResultColumns()
{
	dbbind (_data_base->getDbproc(), 1, NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)_id); 
}

//.=================================================================
void SpDB_DBLib::t_RestoreRow::bindRegularResultColumns()
{
	t_IdRow::bindRegularResultColumns();

	dbbind (_data_base->getDbproc(), 2, NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)_attr);
	dbbind (_data_base->getDbproc(), 3, NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)_value);
}
// END Row definitions
*/
#endif // DB_ENGINE_DBLIB