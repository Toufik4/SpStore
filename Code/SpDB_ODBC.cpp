//#define DB_ENGINE_ODBC
#ifdef DB_ENGINE_ODBC

#include <SpStore/Interface/SpDB_ODBC.h>
#include <string>
#include <SpStore/Interface/SpODBC.h>

using namespace std;
using namespace SpODBC;

SpDB_ODBC::SpDB_ODBC()
{
}

SpDB_ODBC::~SpDB_ODBC()
{
}

//.=================================================================
//.Class : SpDB_ODBC
//.----- 
//. 
//.Member Function : initConnect
//.--------------- 
//. 
//.Description :	m�thode d'initialisation de la connection au  
//.-----------		serveur SQL  
//.					loginName est le nom de login au niveau de la base SQL 
//.					avec son mot de passe loginPasswd
//.					serverName, le nom du serveur SQL et basename la 
//.					base sur laquelle on veut faire les requ�tes. 
//.						
//.Returned Value : si des erreurs existent, elles sont d�rout�es sur
//.--------------	les proc�dures de traitements des erreurs msg_handler
//.					et err_handler
//. 
//.================================================================= 
void SpDB_ODBC::initConnect(const char* loginName, const char* loginPasswd,
	const char* processName, const char* serverName, const char* baseName, const int timeOut)
{

	if (!_Connection.Connect(serverName, loginName, loginPasswd))
    {
		SPSQL_ERROR("Cannot connect to the Data Source : " + _Connection.LastError());
    }

}

bool  SpDB_ODBC::isGoodConnection() const 
	{
        return _Connection.Connected(); 
	}

//.=================================================================
//.Class : SpDB_ODBC
//.----- 
//. 
//.Member Function : closeConnect
//.--------------- 
//. 
//.Description :  deconnexion 
//.----------- 
//. 
//.=================================================================
void SpDB_ODBC::closeConnect()
{
	if (_Connection.Connected())
	{
		SPSQL_INFO("Close SQL Server Connection (ODBC Handle : " << _Connection.dbcHandle() << ")");
		_Connection.Disconnect();
	}
}

//.=================================================================
//.Class : SpDB_ODBC
//.----- 
//. 
//.Member Function : onDisconnection
//.--------------- 
//. 
//.Description :  fonction appel�e en cas de perte du serveur SQL
//.----------- 
//. 
//.=================================================================
void SpDB_ODBC::onDisconnection()
{
	SPSQL_ERROR( "SQL Server broken link" );
	_Connection.Disconnect();

}




// Methodes translated from SpStoreDBManager Class
bool SpDB_ODBC::NextRow(t_Row& row)
{ 
	bool bRet = false;
    if (_Connection.Connected()) {		
		bRet = _Command.FetchNext();
		if(bRet)
			row.bind();
	} 
	row.unLink();
	return bRet;

}

void SpDB_ODBC::bind(t_Row& row)
{
	t_RestoreRow* ptRow = dynamic_cast<t_RestoreRow*>(&row);

	if (ptRow != nullptr){
		if(_Command.count_columns() >= 3){
			std::string id_str = _Command.field(1).as_string();
			strcpy((char*)ptRow->getId(), id_str.c_str());
			std::string attr_str = _Command.field(2).as_string();
			strcpy((char*)ptRow->getAttr(), attr_str.c_str());
			std::string value_str = _Command.field(3).as_string();
			strcpy((char*)ptRow->getValue(), value_str.c_str());
		}
	}
	else{
	    t_IdRow* ptRow = dynamic_cast<t_IdRow*>(&row);
		if(ptRow != nullptr && _Command.count_columns()>= 1){
			std::string id_str = _Command.field(1).as_string();
			strcpy((char*)ptRow->getId(), id_str.c_str());
		}
	}
}
/*
void SpDB_ODBC::bind1(const char* id)
{
	if(_Command.count_columns()>= 1){
		std::string id_str = _Command.field(1).as_string();
		strcpy((char*)id, id_str.c_str());
	}
}

void SpDB_ODBC::bind2(const char* id, const char* attr, const char* value)
{
		if(_Command.count_columns() >= 3){
			std::string id_str = _Command.field(1).as_string();
			strcpy((char*)id, id_str.c_str());
			std::string attr_str = _Command.field(2).as_string();
			strcpy((char*)attr, attr_str.c_str());
			std::string value_str = _Command.field(3).as_string();
			strcpy((char*)value, value_str.c_str());
	}
}
*/
void SpDB_ODBC::AddQuery(const std::string& req)
{ 
	_QList.push_back(req);
} 

void SpDB_ODBC::ExecQuery()
{ 
	if(!_QList.empty()){
		
		std::string delimiter="";
		std::string sRq = "";  //_QList.back(); // list.back(); list.pop_back();

		for (list<string>::iterator rq=_QList.begin(); rq!=_QList.end(); rq++){
				sRq += delimiter + *rq;
				delimiter = "\n";
		}
		if (!_Command.ExecuteDirect(_Connection, sRq))
		{
			SPSQL_ERROR( "Cannot execute query: " + _Command.LastError() );
		}
		_QList.clear();
		//_QList.pop_back();
	}
}

void SpDB_ODBC::CancelQuery()
{ 
	_Command.close();
}

bool SpDB_ODBC::HasLastQuerySucced()
{ 
	return !Failed(); 
}

bool SpDB_ODBC::NoMoreResults()
{ 
	return isGoodConnection() && !_Command.FetchNext(); 
}

bool SpDB_ODBC::Failed()
{ 
	TString sEmpty(""); 
	return _Connection.LastErrorStatusCode() != sEmpty ||  _Command.LastErrorStatusCode() != sEmpty; 
}

#endif // DB_ENGINE_ODBC