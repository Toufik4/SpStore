//*****************************************************************************
//
//  Copyright (c) Siemens Transportation Systems 2003
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpStoreDBMgr.cpp
//  Auteur   : Alexis KOLYBINE
//  Cr�ation : mardi 8 avril 2003
//  Contenu  : D�finition de la classe SpStoreDBMgr
//
//*****************************************************************************
//  Modifications : 
//*****************************************************************************

#include <SpStore/Interface/SpStoreDBMgr.h>
#include <SpStore/Interface/SpStoreTraces.h>
#include <SpStore/Interface/SpStoreRp.h>

const SpCmnIntervalTime SpStoreDBMgr::_frozen_window(SpCmnIntervalTime::Milliseconds(120000));

SpStoreDBMgr* SpStoreDBMgr::_instance = 0;


SpStoreDBMgr::SpStoreDBMgr()
	: _is_initialized(false)
    , _loginName()
    , _password()
    , _processName()
    , _server()
    , _baseName()
    , _reconnectable(false)
   	, _last_connection_try(SpCmnDate::bad)
	, _queueHandler(new SpLogStore())
	, _ptDBEngine(new SpDB_DBLib())
{
}

SpStoreDBMgr::~SpStoreDBMgr()
{
	delete _queueHandler;
	delete _ptDBEngine;
}

SpStoreDBMgr* SpStoreDBMgr::GetInstance()
{
    if (! _instance)
        _instance = new SpStoreDBMgr();

    return _instance;
}

void	SpStoreDBMgr::connect()
{
	_ptDBEngine->initConnect(_loginName.c_str(), _password.c_str(), _processName.c_str(), _server.c_str(), _baseName.c_str());
}

void SpStoreDBMgr::initConnection(const std::string& loginName, const std::string& loginPasswd,
	const std::string& processName, const std::string& serverName, const std::string& baseName, bool reconnectable)
{ 
	_loginName  = loginName;
   _password    = loginPasswd;
   _processName = processName;
   _server      = serverName;
   _baseName    = baseName;
   _reconnectable = reconnectable;

	if (!_is_initialized)
    {
		connect();
		_is_initialized = true; 
	}
}

void SpStoreDBMgr::BeginTransaction()
{ 
	/*
	if (dbproc) 
		dbcmd(dbproc, "BEGIN TRANSACTION\n"); 
	*/
	_ptDBEngine->BeginTransaction();
}

void SpStoreDBMgr::CommitTransaction()
{ 
	/*
	if (dbproc) 
		dbcmd(dbproc, "COMMIT TRANSACTION\n");
	*/
	_ptDBEngine->CommitTransaction();
}

void SpStoreDBMgr::AddQuery(const IlsString& req)
{ 
	/*
	if (dbproc && _ptDBEngine->isGoodConnection()) 
		dbfcmd(dbproc, "%s\n", req.value());
	*/
	_ptDBEngine->AddQuery(req);
}

void SpStoreDBMgr::AddQuery(const std::string& req)
{ 
	/*
	if (dbproc && _ptDBEngine->isGoodConnection()) 
		dbfcmd(dbproc, "%s\n", req.c_str());	
		*/
	_ptDBEngine->AddQuery(req);
}

void SpStoreDBMgr::ExecQuery()
{ 
	/*
	if (dbproc && _ptDBEngine->isGoodConnection()) 
		dbsqlexec(dbproc); 
		*/
	_ptDBEngine->ExecQuery();
}

void SpStoreDBMgr::CancelQuery()
{ 
	/*
	if (dbproc && _ptDBEngine->isGoodConnection()) 
		dbcancel(dbproc);
		*/
	_ptDBEngine->CancelQuery();
}

bool SpStoreDBMgr::HasLastQuerySucced()
{ 
	return _ptDBEngine->HasLastQuerySucced();
	// _ptDBEngine->isGoodConnection() && dbresults(dbproc) == SUCCEED; 
}

bool SpStoreDBMgr::NoMoreResults()
{ 
	return _ptDBEngine->NoMoreResults(); 
}

bool SpStoreDBMgr::Failed()
{ 
	return _ptDBEngine->Failed();
}

void SpStoreDBMgr::closeConnection()
{ 
	_ptDBEngine->closeConnect(); 
	_is_initialized = false; 
}
 
// DBPROCESS* 
bool	SpStoreDBMgr::CheckConnection() // getDbproc()
{
    // si reconnectable sur panne (store doit �tre reconnectable mais restore non : on ne peut bloquer l'init
    // du serveur si la machine d'archivage est en panne
    if (_reconnectable)
    {
        // si la connexion est morte 
        if (!_ptDBEngine->getDbproc())
        {
            // si le dernier essai de reconnexion date de plus de n secondes
			if (_last_connection_try.isBad() || (SpCmnDate::Now() - _last_connection_try  > _frozen_window))
            {
                // essai de reconnexion
                connect();
                _last_connection_try = SpCmnDate::Now();

                // si reconnexion OK
                if (_ptDBEngine->getDbproc())
                {
                    // prise en compte par la repr�sentation
                    SpStoreRepresentation::OnDBReconnection();
                }
            }
        }
    }
	return (_ptDBEngine->getDbproc() != NULL);
}

void SpStoreDBMgr::asyncLog(std::string&& msg)
{
	_queue.push(msg);
}

bool SpStoreDBMgr::waiting_swap_pending_queue(std::deque<std::string>& queue, unsigned long timeout)
{
	return _queue.waiting_swap(queue, timeout);
}
/*
void SpStoreDBMgr::errorSQL(DBPROCESS *dbproc, int severity, int dberr, int oserr, char *dberrstr, char *oserrstr)
{
	SPSTORE_ERROR("DB-LIBRARY ERREUR : " << dberrstr);

	if (oserr != DBNOERR)
	{
		SPSTORE_ERROR("Operating-system error: " << oserrstr );
	}
}
*/