//.=================================================================
//.
//.        Copyright (c) Matra Transport International 1999
//.        Ce fichier est juridiquement prot�g� par la loi 98-531  
//.        du 1er juillet 1998 
//.        La version et les �volutions sont identifi�es dans le  
//.        fichier de version associ� .ver
//.
//.        File : SpStoreMain.cpp
//.        ----
//.
//.        Content :
//.        -------	
//.					
//.
//.=================================================================

#include <SpStore/Interface/SpStoreRestoreRp.h>
#include <SpStore/Interface/SpStoreRp.h>
#include <SpRecon/Interface/SpReconApplication.h>
#include <SpRecon/Interface/SpReconProxyServerInterface.h>

#include <SpCmn/Interface/SpCmnTool.h>
#include <SpCmn/Interface/SpCmnParamAppli.h>
#include <SpCmn/Interface/SpCmnIlsTraceRedirector.h>

#include "Xt/Debugging/CrashHandler.h"
#include <XtWin/Interface/XtWinLogger.h>
#include <XtWin/InfiniteLoop.hpp>

#include <ilserver/mvtcp/tcpmvproc.h>
#include <XtWin/Interface/XtWinDisableGPFErrorBox.h>

extern int SpStoreQPNC;

namespace 
{
	const char* const APPLICATION_NAME = "SpStore";
	const char* const USAGE =
		" [-qnpc <queries-per-notification-cycle>] [-debug|warning|info|error|fatalerror|internalerror|message] [-console]"
		" [-splog <logfile.log>] -spfile <properties-file-1.txt[,properties-file-n.txt]>";

}	// end namespace

extern int main_2(int argc, char **argv);

int main(int argc, char* *argv)
{
	// Positionner DEBUGGING_APPLICATION=1 pour attacher l'application dans le d�bogueur.
	XtWindows::InfiniteLoop infiniteLoop("DEBUGGING_APPLICATION");

	// Inject ODBC
	 // main_2(1, NULL);

//##	Interception des erreurs
	if (!SpCmnTool::IsOptionSet(argc, argv, "ncc"))	// option -ncc (no catch crash)
	{
		Xt::Debugging::CrashHandler::InstallDefault(argc, argv);
	}
	
//##	Prevent the crash window (abnormal program termination) to pop up 
	// si -enable_gpf_box, on ne d�sactive pas la popup syst�me en cas d'erreur anormale
	// 
	// si -disable_gpf_box, on d�sactive la popup syst�me en cas d'erreur anormale
	// 
	// si aucune des options -enable_gpf_box et -disable_gpf_box n'est pr�sente,
	// on d�sactive la popup syst�me en cas d'erreur anormale
	// uniquement si HKLM/SOFTWARE/Microsoft/Windows NT/CurrentVersion/AeDebug/Auto est � 1
	//
	if (false == SpCmnTool::IsOptionSet(argc, argv, "enable_gpf_box"))
	{
	       bool check_aedebug_auto = (false == SpCmnTool::IsOptionSet(argc, argv, "disable_gpf_box"));
	       xtwin::disableGeneralProtectionFaultErrorBox(check_aedebug_auto);
	}


	SpStoreRestoreDummy_ToForceLink SpStoreDummy_ToForceLink_;
	SpStoreDummy_ToForceLink SpStoreDummy_ToForceLink2_;


//##	Initialisation des traces
    std::string application_name(APPLICATION_NAME);
	const char*	argument_name;
	if (true == SpCmnTool::IsOptionSet(argc, argv, "spname", argument_name))
	{
		application_name = argument_name;
	}
	// Dynamic configuration of the traces with logger tool
	XTWIN_RTCONFIG(application_name.c_str());
	XTWIN_INITTRACE(application_name.c_str());

    // redirecteur des traces Ilog Views
	SpCmnIlsTraceRedirector::Install();
	SpCmnTool::InitializeLogFile(argc, argv);


//##	Initialisation des propri�t�s
	if (!SpCmnTool::LoadProperties(argc, argv, USAGE))
	{
		XTWIN_LOGERROR(spocc.store, "Failed to read parameters");
		return 1;
	}
	const SpCmnParamAppli&	properties = SpCmnParamAppli::getInstance();

	const char*	qpnc = 0;
	if (SpCmnTool::IsOptionSet(argc, argv, "qpnc", qpnc))
		SpStoreQPNC = atoi(qpnc);

	std::string date_format = properties.getStringValue("LOG_FORMAT_DATE", "");

	if (date_format != "")
	{
		SpCmnDate::setLocaleFormat(date_format.c_str());
	}


//##	Initialisation du middleware ILOG
	if (!IlsTcpMvProcess::Initialize(argc,argv))
	{
		XTWIN_LOGERROR(spocc.store, "Failed to initialize component process");
		return 1;
	}


//##	Initialisation de l'application (reconnection)
	SpReconApplication		app;
	app.initialise(application_name, properties.getValue("STORE_CONNECTION"), properties);
	app.setLifeSignTimeoutToAllServers(SpReconProxyServerInterface::INFINITE_TIMEOUT);

	XTWIN_LOGNOTE(spocc.store, "SpStore is running...");
	
	IlsTime	cycle_period(500);	// 500ms
	app.run(0, cycle_period);

	return 0;
}


int WINAPI WinMain(HINSTANCE /*hInstance*/,	HINSTANCE /*hPrevInstance*/,
	LPSTR /*lpCmdLine*/, int /*nCmdShow*/)
{
	return main(__argc, __argv);
}

