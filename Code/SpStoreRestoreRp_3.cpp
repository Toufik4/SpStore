//.=================================================================
//.
//.        Copyright (c) Matra Transport International 2000
//.        Ce fichier est juridiquement prot�g� par la loi 98-531  
//.        du 1er juillet 1998 
//.        La version et les �volutions sont identifi�es dans le  
//.        fichier de version associ� .ver
//.
//.        File : SpStoreRestoreRp.cpp
//.        ----
//.
//.        Content : D�finition de la classe SpStoreRestoreRp
//.        -------
//.
//.        Modification :
//.        ------------
//.
//.        ENA 09/12/2003 : Ajout de Rp crees et supprimes dynamiquement.
//.
//.=================================================================
#define SPSTORE_LOG_DEBUG

#include <SpStore/Interface/SpStoreDBMgr.h>
#include <SpStore/Interface/SpStoreRestoreRp.h>
#include <SpStore/Interface/SpStoreTraces.h>		//FIXME store ou restore ?

#include <SpCmn/Interface/SpCmnAccess.h>
#include <SpCmn/Interface/SpCmnDate.h>
#include <SpCmn/Interface/SpCmnMvValueMap.h>
#include <SpCmn/Interface/SpCmnMvValueVector.h>
#include <SpCmn/Interface/SpCmnDirectoryService.h>

#include <ilog/string.h>


SpStoreRestoreDummy_ToForceLink::SpStoreRestoreDummy_ToForceLink() {;}


SpStoreRestoreRepresentation::~SpStoreRestoreRepresentation()
{

}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : endS2CTransaction
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : void 
//.-------------- 
//. 
//.Limitation : 
//.---------- 
//. 
//.=================================================================
void 
SpStoreRestoreRepresentation::endS2CTransaction(IlsS2CTransStatus transStatus, IlsTransactionId transid)
{
	// Si c'est la transaction correspond a une creation (ouverture de vue)
	if (transStatus == ILS_S2C_NOTIFY_CREATION)
	{
		namespace ds = spocc::directory_service;

		// Alors on restaure les valeurs d'attributs des objets serveur.
		SPSTORE_DEBUG("end store trans");

		SPSTORE_INFO("begin restoring from " << _table << "." << _schema << "@" << _host);
	
		// Ouverture de la connection a la base.
		ds::user_account	account = ds::directory::instance().query_database_connection("spocc.restore");
		SpStoreDBMgr::GetInstance()->initConnection(account.user(), account.password(),
			"SpStoreRestore", _host.value(), _schema.value());

		SPSTORE_DEBUG("begin restore trans");

		// Traitement des Rp standards
		int nb_rest = restore();

		SPSTORE_INFO(nb_rest << " restoring requested successfull treated");

		SPSTORE_DEBUG("begin dynamic restore trans");

		// Traitement des Rp crees/supprimes dynamiquement.
		nb_rest = restoreDyn();

		SPSTORE_INFO(nb_rest << " dynamic restoring requested successfull treated");
	}

	IlsRepresentation::endS2CTransaction(transStatus, transid);
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : restore
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Methode execut�e une seule fois.
//.---------- 
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::restore()
{
	SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance();

    // DBPROCESS* dbproc = data_base->getDbproc(); // TAB
    if (data_base->CheckConnection())
    {
		IlsString req;
		// Creation de la requete de selection des attributs a restaurer.
		// => ignore les elements ayant l'attribut 'IS_DYNAMIC'.
		req << "select id, attr, value from " << _table << " where value <> '(null)'"
			<< "and id not in ( select id from " << _table << " where attr = 'IS_DYNAMIC')"; 

        data_base->AddQuery(req); // TAB AddQuery
        data_base->ExecQuery();  // TAB ExecQuery
    }

	if (!data_base->HasLastQuerySucced())  // TAB HasLastQuerySucced
	{
		// La requete a �chou�e
		return 0;
	}

	SpStoreDBMgr::t_RestoreRow row(data_base);
	int nb_rp =	0;

	beginC2STransaction();

	// Pour chaque enregistrement r�cup�r�
	while (data_base->NextRow(row))
	{
		nb_rp++;
		IlsRpObject* rp = 0;

		SPSTORE_DEBUG("Restoring " << row.getId()
			<< "." << row.getAttr() << " with value " 
			<< row.getValue());

		std::map<std::string, SpStoreRestoreRp*>::iterator i = _rp_to_restore.find(row.getId());

		// Si il existe un Rp correspondant a l'enregistrement
		if (i != _rp_to_restore.end()) 
		{
			// Alors le Rp est mis a jour 
			rp = (*i).second;

			updateRpValue( rp, row );
		}
		else
		{
			SPSTORE_WARNING("rp " << row.getId() << " not found. It may be managed by another view (in case of several view defined in StoreView.ils file).");
		}
	}

	commitC2STransaction();
	return nb_rp;
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : restoreDyn
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Methode execut�e une seule fois (a la fin de la
//.----------     transaction d'ouverture de vue).
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::restoreDyn()
{
	// Recuperation dans la base de la liste des collecteurs
	int nb_collector = initDynCollector();

	// Recuperation dans la base de la liste des classes d'objet serveur
	int nb_sv_type = initDynSvTypeName();

	// Recuperation dans la base de la liste des relations entre objet serveur
	int nb_owned = initDynOwnerRelation();

	// Creation des ojetsRp et mise a jour des attributs
	int nb_rp =	restoreDynObject();

	// Mise a jour des relations entre objets (apres qu'ils aient �t� tous cr��s).
	int nb_relation = restoreDynRelation();

	return nb_rp;
}

//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : restoreDynObject
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation : 
//.----------
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::restoreDynObject()
{
	_dynRp_to_restore.clear();

	SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance();

    // DBPROCESS* dbproc = data_base->getDbproc(); // TAB
    if (data_base->CheckConnection())
    {
		IlsString req;
		// Creation de la requete de selection des attributs a restaurer.
		// => selectionne les elements ayant l'attribut 'IS_DYNAMIC'.
		// => ignore les attributs 'SV_TYPE', 'COLLECTOR' et 'HAS_OWNER'.
		req << "select id, attr, value from " << _table << " where value <> '(null)'"
			<< " and attr not in ('SV_TYPE', 'COLLECTOR', 'HAS_OWNER')" 
			<< " and id in ( select id from " << _table << " where attr = 'IS_DYNAMIC')"; 

        data_base->AddQuery(req); // TAB AddQuery
	    data_base->ExecQuery(); // TAB ExecQuery
    }
	if (!data_base->HasLastQuerySucced()) // TAB HasLastQuerySucced
	{
		// La requete a �chou�e
		return 0;
	}
	
	SpStoreDBMgr::t_RestoreRow row(data_base);
	int nb_rp =	0;

	// Pour chaque enregistrement r�cuper� :
	//    - creation du Rp correspondant si il n'existe pas
	//    - mise a jour systematique de l'attribut defini par l'enregistrement
	while (data_base->NextRow(row))
	{
		if (nb_rp == 0)
			beginC2STransaction();
		nb_rp++;
		SPSTORE_DEBUG("Restoring " << row.getId()
			<< "." << row.getAttr() << " with value " 
			<< row.getValue());
		// On recherche l'existence prealable du Rp
		std::map<std::string, SpStoreRestoreDynRp*>::iterator it_rp = _dynRp_to_restore.find(row.getId());
		SpStoreRestoreDynRp* rp = 0;
		// Si il existe deja
		if (it_rp != _dynRp_to_restore.end()) 
		{
			// Alors on recupere le pointeur sur le Rp.
			rp = (*it_rp).second;
		}
		else
		{
			// Sinon on le cr�e
			SPSTORE_DEBUG("Dynamic rp not found, then created in server");
			rp = createDynRp( row.getId() );
		}

		// Mise a jour sysematique de l'attribut.
		updateRpValue( rp, row );
	}

	if (nb_rp != 0)
		commitC2STransaction();

	return nb_rp;
}

//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : restoreDynRelation
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Doit etre executer apres la methode 'restoreDynObject'.
//.----------
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::restoreDynRelation()
{
	int nb_rel =	0;
	std::map<std::string, std::string>::iterator it_rel_rp;

	// Pour chaque relation
	for ( it_rel_rp = _ownerRelationMap.begin(); it_rel_rp != _ownerRelationMap.end(); ++it_rel_rp)
	{
		SpStoreRestoreDynRp* object_rp = 0;
		SpStoreRestoreDynRp* owner_rp  = 0;
		std::string object_id    = (*it_rel_rp).first;
		std::string owner_id     = (*it_rel_rp).second;
		std::string collectorId  = _collectorIdMap[ object_id ];
		// On recherche le Rp de l'objet et le Rp du owner associ�.
		std::map<std::string, SpStoreRestoreDynRp*>::iterator it_objet = _dynRp_to_restore.find( object_id );
		std::map<std::string, SpStoreRestoreDynRp*>::iterator it_owner = _dynRp_to_restore.find( owner_id );
		// Si les 2 objets sont referenc�s
		if ( (it_objet != _dynRp_to_restore.end()) && (it_owner != _dynRp_to_restore.end()) )
		{
			// Alors on recupere les Rp
			object_rp = (*it_objet).second;
			owner_rp = (*it_owner).second;
			// Si les 2 Rp existent
			if ( (owner_rp != 0) && (object_rp != 0) )
			{
				if (nb_rel == 0)
					beginC2STransaction();

				// Alors on met a jour la relation entre les Rp (repercut�e sur le serveur)
				IlsRpAttributeId collectorAttrId = owner_rp->getModel()->getAttrId(collectorId.c_str());
				object_rp->onAddToCollection( *owner_rp, collectorAttrId );
				nb_rel++;
			}
		}
	}

	if (nb_rel != 0)
		commitC2STransaction();

	return nb_rel;
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : initDynCollector
//.--------------- 
//. 
//. 
//.Description :  Recupere la liste des collecteur d'objet dynamique
//.-----------    d�finis dans la base de store
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Methode execut�e une seule fois. 
//.---------- 
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::initDynCollector()
{
	SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance();

    // DBPROCESS* dbproc = data_base->getDbproc(); // TAB
    if (data_base->CheckConnection())
    {
		IlsString req;
		req << "select id, attr, value from " << _table << " where attr = 'COLLECTOR'"; 

		data_base->AddQuery(req); // TAB AddQuery
	    data_base->ExecQuery(); // TAB ExecQuery
    }

	if (!data_base->HasLastQuerySucced()) // TAB HasLastQuerySucced
		return 0;

	SpStoreDBMgr::t_RestoreRow row(data_base);
	int nb_rp =	0;

	while (data_base->NextRow(row))
	{
		nb_rp++;

		SPSTORE_DEBUG("Retrieving " << row.getId()
			<< "." << row.getAttr() << " with value " 
			<< row.getValue());

		addCollectorId( row.getId(), row.getValue() );
	}

	return nb_rp;
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : initDynSvTypeName
//.--------------- 
//. 
//. 
//.Description :  Recupere la liste des classes d'objet dynamique
//.-----------    d�finis dans la base de store
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Methode execut�e une seule fois. 
//.---------- 
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::initDynSvTypeName()
{
    SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance();

    // DBPROCESS* dbproc = data_base->getDbproc(); // AB
    if (data_base->CheckConnection())
    {
		IlsString req;
		req << "select id, attr, value from " << _table << " where attr = 'SV_TYPE'"; 

		data_base->AddQuery(req); // TAB AddQuery
	    data_base->ExecQuery(); // TAB ExecQuery
    }

	if (!data_base->HasLastQuerySucced()) // TAB HasLastQuerySucced
		return 0;

	SpStoreDBMgr::t_RestoreRow row(data_base);
	int nb_rp =	0;

	while (data_base->NextRow(row))
	{
		nb_rp++;

		SPSTORE_DEBUG("Retrieving " << row.getId()
			<< "." << row.getAttr() << " with value " 
			<< row.getValue());

		addSvTypeName( row.getId(), row.getValue() );
	}

	return nb_rp;
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : initDynOwnerRelation
//.--------------- 
//. 
//. 
//.Description :  Recupere la liste des relations entre objets dynamiques
//.-----------    d�finis dans la base de store
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Methode execut�e une seule fois. 
//.---------- 
//. 
//.=================================================================
int
SpStoreRestoreRepresentation::initDynOwnerRelation()
{
    SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance();

    // DBPROCESS* dbproc = data_base->getDbproc();
    if (data_base->CheckConnection())
    {
		IlsString req;
		req << "select id, attr, value from " << _table << " where attr = 'HAS_OWNER'"; 

        data_base->AddQuery(req); // TAB AddQuery
	    data_base->ExecQuery(); // TAB ExecQuery
    }

	if (!data_base->HasLastQuerySucced()) // TAB HasLastQuerySucced
		return 0;

	SpStoreDBMgr::t_RestoreRow row(data_base);
	int nb_rp =	0;

	while (data_base->NextRow(row))
	{
		nb_rp++;

		SPSTORE_DEBUG("Retrieving " << row.getId()
			<< "." << row.getAttr() << " with value " 
			<< row.getValue());

		addOwnerRelation( row.getId(), row.getValue() );
	}

	return nb_rp;
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : addDynRpToRestore
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :   Methode execut�e une seule fois. 
//.---------- 
//. 
//.=================================================================
void SpStoreRestoreRepresentation::addDynRpToRestore(const std::string& id, SpStoreRestoreDynRp* rp)
{
	if ( rp != 0 )
	{
		std::map<std::string, SpStoreRestoreDynRp*>::iterator i = _dynRp_to_restore.find(id);

		if (i == _dynRp_to_restore.end()) 
		{
			_dynRp_to_restore[id] = rp;
		}
	}
}

//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : createDynRp
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation : 
//.---------- 
//. 
//.=================================================================
SpStoreRestoreDynRp*
SpStoreRestoreRepresentation::createDynRp(const IlsString& rowId)
{
	std::string svTypeName         = _svTypeNameMap[ rowId.value() ];
	std::string collectorId        = _collectorIdMap[ rowId.value() ];
	std::string ownerId            = _ownerRelationMap[ rowId.value() ];

	SpStoreRestoreOwnerRp* owner   = _ownerRpMap[ ownerId ];
	SpStoreRestoreDynRp* new_rp    = 0;

	const IlsRpObjModel* rp_model  = getModel().getObjModel( svTypeName.c_str(), "object_dyn_rp" );

	if ( rp_model != 0 )
	{
		// Si il existe un Owner prealablement defini (objet non dynamic => de type SpSrvObject)
		if ( owner != 0)
		{
			if ( collectorId != "" )
			{
				// Alors on cr�e le Rp avec la relation au Owner
				IlsRpAttributeId collectorAttrId = owner->getModel()->getAttrId(collectorId.c_str());
				if (collectorAttrId != -1)
				{
					new_rp = new SpStoreRestoreDynRp( *owner, collectorAttrId, rp_model, IlsTrue );
				}
				else
				{
					SPSTORE_ERROR( "'" << rowId << "' has invalid collector attribute");
				}
			}
			else
			{
				SPSTORE_ERROR( "'" << rowId << "' has no collector");
			}
		}
		else
		{
			// Sinon on cr�e un Rp sans relation
			new_rp = new SpStoreRestoreDynRp( *this, *rp_model );

			SPSTORE_DEBUG("'" << rowId << "' created without owner");
		}
	}

	if ( new_rp == 0 )
	{
		SPSTORE_INFO("Unable to create '" << rowId << "'. This is not an error if it is managed by another view (in case of several view defined in StoreView.ils file).");
	}
	else
	{
		// Mise a jour de l'identifiant pour la creation de l'objet serveur
		new_rp->identify( rowId.value() );

		int attr_id = new_rp->getModel()->getAttrId("id");
		if (attr_id == -1)
		{
			SPSTORE_ERROR("Attribute " << rowId.value() << ".id not found in model. Check .ils");
		}
		else
		{
			IlsBoolean res = new_rp->onUpdate(attr_id, rowId.value());
		}

		// Le Rp est referenc� pour ne pas etre recr��.
		addDynRpToRestore( rowId.value(), new_rp );
	}

	return new_rp;
}


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : updateRpValue
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation : 
//.---------- 
//. 
//.=================================================================
void
SpStoreRestoreRepresentation::updateRpValue( IlsRpObject* rp, SpStoreDBMgr::t_RestoreRow& row )
{
	if ( rp != 0 )
	{
		IlsRpAttributeId attr_id = rp->getModel()->getAttrId(row.getAttr());

		if (attr_id == -1)
		{
			SPSTORE_ERROR("Attribute " << row.getId() << "." << row.getAttr()
					   << " not found in model. Check database or .ils");
		}
		else
		{
			if (!rp->isEditable(attr_id))
			{
				SPSTORE_ERROR("Attribute " << row.getId() << "." << row.getAttr()
							  << " is NOT an editable attribute, check the .ils");
			}

			IlsMvDataType::Kind rpType = rp->getModel()->getAttrModel(attr_id)->getDataType().getKind();

			try
			{
				switch (rpType)
				{
					case IlsMvDataType::MV_BOOL  :
						rp->onUpdate(attr_id, strcmp(row.getValue(),"true") == 0);
						break; 

					case IlsMvDataType::MV_CHAR  :
						rp->onUpdate(attr_id, *row.getValue());
						break;

					case IlsMvDataType::MV_LONG  :
						rp->onUpdate(attr_id, atol(row.getValue()));
						break;

					case IlsMvDataType::MV_FLOAT :
						rp->onUpdate(attr_id, atof(row.getValue()));
						break;

					case IlsMvDataType::MV_DOUBLE :
						rp->onUpdate(attr_id, atof(row.getValue()));
						break;

					case IlsMvDataType::MV_STRING  :
						rp->onUpdate(attr_id, row.getValue());
						break;

					case IlsMvDataType::MV_USER  :
						{
							IlsString user_type_name(rp->getModel()->getAttrModel(attr_id)->getDataType().getUserTypeName());
							if (user_type_name == "SpCmnAccess") 
							{
								SpCmnAccess as_access(row.getValue(), 0, IlsFalse);
								rp->onUpdate(attr_id, as_access);
							}
							else
							{
								if (user_type_name == "SpCmnDate")
								{
									SpCmnDate dateRestore = SpCmn::MakeDate(row.getValue(), 0);
									rp->onUpdate(attr_id, dateRestore);
								}
								else if (user_type_name == "SpCmnMvValueMap")
								{
									rp->onUpdate(attr_id, SpCmnMvValueMap::fromString(row.getValue()));
								}
								else if (user_type_name == "SpCmnMvValueVector")
								{
									rp->onUpdate(attr_id, SpCmnMvValueVector::fromString(row.getValue()));
								}
								else if (user_type_name == "SpCmnIntervalTime")
								{
									rp->onUpdate(attr_id, SpCmnIntervalTime::fromString(row.getValue()));
								}
								else
								{
									SPSTORE_ERROR("Unable to decode type of user type " << user_type_name
										<< " : " << row.getId() << "." << row.getAttr());
								}
							}
						}
						break;


					default :
						SPSTORE_ERROR("Unable to decode type of "
							<< row.getId() << "." << row.getAttr());
						break;
				}
			}
/*			catch (const std::exception& ex)
			{
				SPSTORE_ERROR("Unable to decode data of "
					<< row.getId() << "." << row.getAttr() << " : "  << row.getValue() << " - " << ex.what());
			}
*/
			catch (...)
			{
				SPSTORE_ERROR("Unable to decode data of "
					<< row.getId() << "." << row.getAttr() << " : "  << row.getValue());
			}
		}
	}
	else
	{
		SPSTORE_INFO("Unable to update attribute " << row.getId() << "." << row.getAttr()
				   << " because Rp object pointer is null. This is not an error if it is" 
				   << " managed by another view (in case of several view defined in StoreView.ils file).");
	}
}
		

ILS_REPRESENTATION1_BEGIN(SpStoreRestoreRepresentation, SpStoreRepresentation)
ILS_REPRESENTATION_END(SpStoreRestoreRepresentation)


//// ----------------------------------------------------------------------------------------------
//// ----------------------------------------------------------------------------------------------

//.=================================================================
//.Class : SpStoreRestoreRp
//.----- 
//. 
//.Member Function : setId
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : void 
//.-------------- 
//. 
//.Limitation : 
//.---------- 
//. 
//.=================================================================
void SpStoreRestoreRp::setId(const IlsString &id)  
{ 
	_id  = id;

	if (SpStoreRestoreRepresentation* rep = dynamic_cast<SpStoreRestoreRepresentation*>(getRepresentation()))
		rep->addRpToRestore(this);
}

ILS_RP_OBJECT_BEGIN(SpStoreRestoreRp)
ILS_RP_ATTR_STRING(SpStoreRestoreRp,id,setId)
ILS_RP_OBJECT_END(SpStoreRestoreRp)

//// ----------------------------------------------------------------------------------------------
//// ----------------------------------------------------------------------------------------------

			
//.=================================================================
//.Class : SpStoreRestoreDynRp
//.----- 
//. 
//.Member Function : identify
//.--------------- 
//.  Sauvegarde dans l'instance son identifiant unique tel qu'il est dans la BDD.
//.
//.Returned Value : void 
//.-------------- 
//. 
//.Limitation : 
//.---------- 
//. 
//.=================================================================
void SpStoreRestoreDynRp::identify(const IlsString& id)
{
	_id = id;
}


ILS_RP_OBJECT1_BEGIN(SpStoreRestoreDynRp, SpStoreRestoreOwnerRp)
ILS_RP_OBJECT_END(SpStoreRestoreDynRp)


//// ----------------------------------------------------------------------------------------------
//// ----------------------------------------------------------------------------------------------

//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Member Function : setId
//.--------------- 
//. 
//. 
//.Description :  >>> ALL the parameters must be described here <<<
//.----------- 
//. 
//.Returned Value : 
//.-------------- 
//. 
//.Limitation :    Cette m�thode est aussi appell�e par les objets
//.----------      dynamiques (SpStoreRestoreDynRp), mais trop tard
//.                pour etablir les relations entre objets a partir
//.                de l'appel de 'restoreDynRelation()'.
//. 
//.=================================================================
void 
SpStoreRestoreOwnerRp::setId(const IlsString &id)  
{ 
	_id  = id;

	if (SpStoreRestoreRepresentation* rep = dynamic_cast<SpStoreRestoreRepresentation*>(getRepresentation()))
		if (! _id.isEmpty())
			rep->addOwnerRp(this);
}

ILS_RP_OBJECT_BEGIN(SpStoreRestoreOwnerRp)
ILS_RP_ATTR_STRING(SpStoreRestoreOwnerRp,id,setId)
ILS_RP_OBJECT_END(SpStoreRestoreOwnerRp)
