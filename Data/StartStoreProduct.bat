@ECHO OFF
::
:: Copyright (c) Siemens SAS IC MOL 2011
:: Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
::
:: Fichier de lancement multi-projet
::
:: Fichier appele avec parametre : %1 nom de l'executable %2 numero du serveur, %3 type de lancement (Delivery, Development)
:: le premier parametre est obligatoire
:: le second parametre est obligatoire
:: Si le troisieme param�tre n'est pas donn�, le lancement se fera en Delivery (i.e site)
::
:: IMPORTANT : la variable PROJECT_PREFIX doit etre obligatoirement d�finie AVANT l'appel de ce fichier 
::

SETLOCAL ENABLEDELAYEDEXPANSION
IF NOT DEFINED PROJECT_PREFIX (
echo "************ PROJECT_PREFIX must be defined. START ABORTED*************"
pause
exit)

SET SERVER=%1
IF NOT DEFINED SERVER (
echo "************ SERVER number must be given. START ABORTED*************"
pause
exit)

SET CONFIG=%2
IF NOT x%CONFIG%==xDevelopment SET CONFIG=Delivery

SET SERVER_NAME=%3

:: for Ilog Server 5.2 Patch 16
SET ILS_TCP_PACKET_SIZE=4194304

REM Home for data/view.dbm
IF NOT DEFINED ILSHOME SET ILSHOME=D:\%PROJECT_PREFIX%\Ilog\Server

REM Add more traces in case of TCP exception (file name and line number where exception is thrown). Valid for Rogue Wave Server > 5.6 and with mvtcp.lib patch
set ILS_TCP_VERBOSE_EXCEPTION_MESSAGE=1

REM Application and data paths
IF NOT DEFINED STORE_BIN_PATH SET STORE_BIN_PATH=D:\%PROJECT_PREFIX%\Ats
IF NOT DEFINED STORE_CONFIG_PATH SET STORE_CONFIG_PATH=D:\%PROJECT_PREFIX%\Ats
IF NOT DEFINED PRODUCT_CONFIG_PATH SET PRODUCT_CONFIG_PATH=D:\%PROJECT_PREFIX%\Ats

:: Maximum number of log files / Number of lines per log files
:: Default hexa values are: 00000384 / 00004e20
IF NOT DEFINED STORE_MAX_NB_LOGFILES SET STORE_MAX_NB_LOGFILES=00000384
IF NOT DEFINED STORE_MAX_NB_LOGLINES SET STORE_MAX_NB_LOGLINES=00004e20

SET SPDEF=
SET ILSN=
SET ILOG_TRACE_LEVEL=
:: Surcharge des variables pour le lancement dans l'environnement de developpement
IF %CONFIG% == Development (
    @SET SPDEF=!SPDEF! -spdef CONFIG_DATA_FOLDER=%STORE_CONFIG_PATH%
    @SET SPDEF=!SPDEF! -spdef COMMON_CONFIG_DATA_FOLDER=%COMMON_CONFIG_PATH%
    @SET SPDEF=!SPDEF! -spdef store.database.hostname=FRCTN1A003APP
)

SET CONNECTION_FILE=%PROJECT_PREFIX%StoreConnection_%SERVER%.txt
SET APPLICATION_NAME=%PROJECT_PREFIX%Store%SERVER%

:: Define the server name required variables if we are in case of multiple ATS server instances
IF DEFINED SERVER_NAME (
    SET SPDEF=!SPDEF! -spdef SERVER_NAME=%SERVER_NAME%
    SET CONNECTION_FILE=%PROJECT_PREFIX%%SERVER_NAME%StoreConnection_%SERVER%.txt
    SET APPLICATION_NAME=%PROJECT_PREFIX%%SERVER_NAME%Store%SERVER%
)

SET SPDEF=!SPDEF! -spdef STORE_CONNECTION=%STORE_CONFIG_PATH%\%PROJECT_PREFIX%%SERVER_NAME%StoreConnection_%SERVER%.txt

:: ----------------------------------------------------------------------------
:: Definit la variable CRASHLOG_FOLDER et cree le repertoire si pas existant
CALL %PRODUCT_CONFIG_PATH%\StartConfigLoggerProduct.bat %APPLICATION_NAME% %STORE_MAX_NB_LOGFILES% %STORE_MAX_NB_LOGLINES%

:: Lancement de l'application
START /MIN %STORE_BIN_PATH%\SpStore.exe %ILOG_SERVER_TRACE% ^
    -dump full -warning ^
	-dumpdir %CRASHLOG_FOLDER% ^
    -spname %APPLICATION_NAME% ^
    %SPDEF% ^
    -spfile %STORE_CONFIG_PATH%\%PROJECT_PREFIX%StoreConfiguration.properties

REM ----------------------------------------------------------------------------
@ENDLOCAL
