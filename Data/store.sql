if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[store]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[store]
GO

if exists (select * from dbo.systypes where name = N't_id')
exec sp_droptype N't_id'
GO

if exists (select * from dbo.systypes where name = N't_label')
exec sp_droptype N't_label'
GO

setuser
GO

EXEC sp_addtype N't_id', N'varchar (32)', N'not null'
GO

setuser
GO

setuser
GO

EXEC sp_addtype N't_label', N'varchar (32)', N'not null'
GO

setuser
GO

CREATE TABLE [dbo].[store] (
	[date] [datetime] NOT NULL ,
	[id] [t_id] NOT NULL ,
	[attr] [t_label] NOT NULL ,
	[value] [t_label] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[store] WITH NOCHECK ADD 
	CONSTRAINT [PK_store] PRIMARY KEY  CLUSTERED 
	(
		[id],
		[attr]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

