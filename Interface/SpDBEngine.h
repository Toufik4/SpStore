#pragma once

#define STORE_MANAGER_MAX_ID_LENGTH 256
#define STORE_MANAGER_MAX_ATTR_LENGTH 256
#define STORE_MANAGER_MAX_VALUE_LENGTH 256

#include <SpStore/Interface/SpDBEngine.h>
//#include <SpStore/Interface/SpStoreTraces.h>
#include <ilserver/sstring.h>
#include <SpCmn/Interface/SpCmnAssertion.h>
// #include <SpCmn/Interface/SpCmnApplicationPolicies.h>
#include <SpCmn/Interface/SpCmnParamAppli.h>
#include <XtWin/Interface/XtWinThreading.h>


//FIXME creer un fichier d'include
// D�sactivation des traces si SPSTORE_LOG_DEBUG n'est pas d�finie
#ifndef SPSQL_LOG_DEBUG
#  define SPSQL_NDEBUG
#endif

// traces 'spocc.sql'
#ifndef SPSQL_NDEBUG
#  define SPSQL_DEBUG(msg)				XTWIN_LOGDEBUG(spocc.sql, msg)
#else
#  define SPSQL_DEBUG(msg)
#endif

#define SPSQL_TRACE(msg)				XTWIN_LOGTRACE(spocc.sql, msg)
#define SPSQL_INFO(msg)					XTWIN_LOGINFO(spocc.sql, msg)
#define SPSQL_NOTE(msg)					XTWIN_LOGNOTE(spocc.sql, msg)
#define SPSQL_WARNING(msg)				XTWIN_LOGWARNING(spocc.sql, msg)
#define SPSQL_ERROR(msg)				XTWIN_LOGERROR(spocc.sql, msg)


// traces conditionnelles 'spocc.sql'
#ifndef SPSQL_NDEBUG
#  define SPSQL_DEBUGIF(pre, msg)		XTWIN_LOGDEBUGIF(spocc.sql, pre, msg)
#else
#  define SPSQL_DEBUGIF(pre, msg)
#endif

#define SPSQL_TRACEIF(pre, msg)			XTWIN_LOGTRACEIF(spocc.sql, pre, msg)
#define SPSQL_INFOIF(pre, msg)			XTWIN_LOGINFOIF(spocc.sql, pre, msg)
#define SPSQL_NOTEIF(pre, msg)			XTWIN_LOGNOTEIF(spocc.sql, pre, msg)
#define SPSQL_WARNINGIF(pre, msg)		XTWIN_LOGWARNINGIF(spocc.sql, pre, msg)
#define SPSQL_ERRORIF(pre, msg)			XTWIN_LOGERRORIF(spocc.sql, pre, msg)
#define SPSQL_FATALERRORIF(pre, msg)	XTWIN_LOGFATALERRORIF(spocc.sql, pre, msg)

class SpDBEngine
{
public :
	// BEGIN Row Definitions
	class t_Row {
	public:
		t_Row();
		//##Documentation
		//## bind request results to this class attributes.
		virtual void bind() {};
		void unLink(){ _bound = false;}
	protected:
		//##Documentation
		//## Indique si le lien a �t� �tabli entre les attributs de la classe et les r�sultats d'une requ�te sur la base.
		bool _bound;
	};

	//##Documentation
	//## Permet de r�cup�rer les r�sultats d'une requ�te SQL qui porte sur des identifiants seulement.
	class t_IdRow : public t_Row {
	public:
		//##Documentation
		//## Constructeur.
		t_IdRow(SpDBEngine* data_base);

		//##Documentation
		//## bind request results to this class attributes.
		void bind();
		//##Documentation
		//## Identifiant d'objet du serveur dans la base Store.
		const char* getId() const { return _id; }

	protected:

		//##Documentation
		//## bind request columns to this class attributes.
		virtual void bindRegularResultColumns();

		//##Documentation
		//## Identifiant d'objet du serveur dans la base Store.
		char _id[STORE_MANAGER_MAX_ID_LENGTH];


		//##Documentation
		//## lien vers le manager de BD.
		SpDBEngine* _data_base;

	};

	//##Documentation
	//## Permet de r�cup�rer les r�sultats d'une requ�te SQL qui porte sur les identifiants, les noms des attributs et leurs valeurs.
	class t_RestoreRow : public t_IdRow { 
	public:
		//##Documentation
		//## Constructeur.
		t_RestoreRow(SpDBEngine* data_base);

		//##Documentation
		//## Nom d'attribut d'un objet RP dans la base Store.
		const char* getAttr() const {
			return _attr; 
		}

		//##Documentation
		//## Valeur associ�e � un attribut d'un objet RP dans la base Store.
		const char* getValue() const {
			return _value; 
		}

	protected:
		//## bind request columns to this class attributes.

		//##Documentation
		virtual void bindRegularResultColumns();

	private:
		//##Documentation
		//## Nom d'attribut d'un objet RP dans la base Store.
		char _attr[STORE_MANAGER_MAX_ATTR_LENGTH];

		//##Documentation
		//## Valeur associ�e � un attribut d'un objet RP dans la base Store.
		char _value[STORE_MANAGER_MAX_VALUE_LENGTH];
	};

	friend t_Row;

	friend t_RestoreRow;
	// END Row Definitions

	// Rows Manipulation
	//bool NextRow(t_Row& row);

	SpDBEngine(){};
	//##Documentation
	//## Destructeur.
	virtual ~SpDBEngine(){};

	//##Documentation
	//## Ouverture de la connection SQL serveur.
	virtual void initConnect(const char* loginName, const char* loginPasswd,
		const char* processName, const char* serverName, const char* baseName, const int timeOut = 1) = 0;
	
	//##Documentation
	//## fermeture de la connection SQL serveur. 
	virtual void closeConnect() = 0; 


    virtual bool isGoodConnection() const = 0;
    

	// Wrapping DBLib API Routines :
	virtual void BeginTransaction(){};
	virtual void CommitTransaction(){} ;
	virtual void AddQuery(const IlsString& req) { AddQuery(std::string(req.value()));}
	virtual void AddQuery(const std::string& req) = 0;
	virtual void ExecQuery() = 0;
	virtual void CancelQuery() = 0;
	virtual bool HasLastQuerySucced() = 0;
	virtual bool NoMoreResults() = 0;
	virtual bool Failed() = 0;


	// Rows Manipulation
	virtual bool NextRow(t_Row& row) = 0;
	//virtual void bind1(const char* id) = 0;
	//virtual void bind2(const char* id, const char* att, const char* value) = 0;
	virtual void bind(t_Row& row) = 0;
protected: 

	//##Documentation
	//## Fonction appel�e en cas de perte du serveur SQL.
	virtual void onDisconnection() = 0;
	

private : 

};
