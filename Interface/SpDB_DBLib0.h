#pragma once

//*****************************************************************************
//
//  Copyright (c) Matra Transport International 2001
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpDB_DBLib.h
//  Auteur   : Jean BRETAUDEAU
//  Cr�ation : jeudi 13 septembre 2001
//  Contenu  : D�claration de la classe SpDB_DBLib
//
//*****************************************************************************
//  Modifications : 21335_OT_1 : A. Kolybine : Gestion des deconnexions SQL Server.
//								 Rajout de la m�thode virtuelle onDisconnection()
//*****************************************************************************
#ifndef DB_ENGINE_DBLIB
#define DB_ENGINE_DBLIB
#endif

#ifdef DB_ENGINE_DBLIB

#ifndef DBNTWIN32
#define DBNTWIN32
#endif

#include <XtWin/Interface/XtWinWindowsApi.h>
#include <sqlfront.h>
#include <sqldb.h>
#include <ilserver/sstring.h>

//.=================================================================
//.Class : SpDB_DBLib 
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0	28/01/99 Creation : Marie-H�l�ne Orluc
//.			13/02/01 Modif : MHO . La classe est transform�e en classe 
//.					 singleton. Les anciennes m�thodes statiques sont gard�es
//;					 pour des raisons de compatibilit� 
//. 
//.Inheritance Path : none
//.---------------- 
//. 
//.Description : ensemble de m�thodes permettant le cr�ation des instances
//.-----------	 du mod�le de donn�es SPOCC � partir de table SQL.
//. 
//.Limitation : %%%TBD
//.---------- 
//. 
//.=================================================================
class SpDB_DBLib
{
public :
	//##Documentation
	//## Destructeur.
	virtual ~SpDB_DBLib();

	//##Documentation
	//## Ouverture de la connection SQL serveur.
	virtual void initConnect(const char* loginName, const char* loginPasswd,
		const char* processName, const char* serverName, const char* baseName, const int timeOut = 1);
	
	//##Documentation
	//## fermeture de la connection SQL serveur. 
	virtual void closeConnect();

	//##Documentation
	//## Ex�cution de la requ�te SQL 
	virtual DBPROCESS* executeQuery(const char* command); 

	virtual DBPROCESS* getDbproc() {
		return _dbproc; }

    bool isGoodConnection() const {
        return _dbproc != 0; }
    
	//##Documentation
    //## D�but de transaction  : m�thode � appeler avant tout groupe  
	//## de requ�tes d'�criture ou de mise � jour � �x�cuter d'un seul bloc.
	void begin_transaction();

	//##Documentation
	//## Fin de transaction : � utiliser � la fin de chaque fin de groupes 
	//## de transactions. 
	void commit_transaction();

	//##Documentation
	//## Indique si une transaction est en cours.
	bool isTransactionEnCours()	const {
		return _transaction_en_cours; }

	//##Documentation
	//## Positionne l'indicateur de transaction en cours.
	void setTransactionEnCours(bool transaction_en_cours) {
		_transaction_en_cours = transaction_en_cours; }

	SpDB_DBLib();

	// Wrapping DBLib API Routines :
	void BeginTransaction();
	void CommitTransaction();
	void AddQuery(const IlsString& req);
	void AddQuery(const std::string& req);
	void ExecQuery();
	void CancelQuery();
	bool HasLastQuerySucced();
	bool NoMoreResults();
	bool Failed();
	// virtual DBPROCESS* getDbproc();

protected: 

	//##Documentation
	//## Fonction appel�e en cas de perte du serveur SQL.
	virtual void onDisconnection();
	
	//##Documentation
	//## Gestion des erreurs SQL  : appelant la m�thode errorSQL pouvant �tre d�riv�e 
	static	 int err_handler(DBPROCESS*, int, int, int, char*, char*);
	//##Documentation
	//## Gestion des erreurs SQL.
	virtual void errorSQL(DBPROCESS*, int, int, int, char*, char*);

	// pointeur sur la structure utilis�e pour l'utilisation des 
	// DB library 
	DBPROCESS*	_dbproc; 

    // pour fermer l'ancienne connexion avant l'ouverture d'une nouvelle connexion 
    // Remarque : la fermeture de l'ancienne connexion est retard�e pour �viter qu'une
    // utilisation soit faite d'un dbproc avec une connexion ferm�e ...
   	DBPROCESS*	_old_dbproc;

	bool _transaction_en_cours;

	static bool _is_initialised;

private : 

};

#endif DB_ENGINE_DBLIB