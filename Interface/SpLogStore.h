#pragma once

//.=================================================================
//.Class : SpLogStore
//.----- 
//.Inheritance Path : 
//.---------------- 
//. XtWinThread 
//. -> SpLogStore
//. 
//.Description : This class is a thread object whose role is to empty SpStoreDBManager's request queue and execute the requests stored in it.
//.----------- 
//.=================================================================

#include <XtWin/Interface/XtWinThread.h>

class SpLogStore : private XtWinThread
{
public:

	//##Documentation
	//## Constructor. Starts the thread's function
	SpLogStore();

	//##Documentation
	//## Destructor. In parent's destructor, the thread is terminated
	virtual ~SpLogStore();

private:
	//##Documentation
	//## Thread's function (overrides XtWinThread's do_run() method)
	virtual unsigned long do_run();

	//##Documentation
	//## Unexpected interruption event
	virtual void on_abort(unsigned exit_code);

};