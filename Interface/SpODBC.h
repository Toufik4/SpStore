#ifdef DB_ENGINE_ODBC

#ifndef _SPODBC_H_DEFINED_
#define _SPODBC_H_DEFINED_

// System headers
#ifdef _WIN32
#include <windows.h>
#else
#define TCHAR char
#endif

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

// STL Headers
#include <string>
#include <map>

//! The only one namespace of SpODBC
/**
	Everything is well organized under this namespace
*/
namespace SpODBC
{
	//! MACRO for std::wstring or std::string based on _UNICODE definition.
	#ifdef _UNICODE
		typedef std::wstring TString;	
	#else
		typedef std::string TString;
	#endif

	// Class prototypes
	class Connection;
	class Field;
	class Param;
	class Command;	

	//! @name Library Version
	//! @{

	//! Get current major version of SpODBC library
	/**
		The major version is increased only when we
		have major changes.
	@return The actual version of the linked library.
	*/
	unsigned short version_major();

	//! Get current minor version of SpODBC library
	/**
		The minor version is increased when new features
		are added/removed or API breakage occurs.
	@return The actual version of the linked library.
	*/
	unsigned short version_minor();

	//! Get current revision of this version
	/**
		The version revision number is changed only
		for bug fixes.
	@return The actual version of the linked library.
	*/
	unsigned short version_revision();

	//! @}

	//! An ODBC Connection representation object
	/**
		Connection object is implementing the actual Connection
		as an ODBC Client, this object can be used from Command
		to perform queries on this Connection.
	@note SpODBC::Connection is <B>Uncopiable</b> and <b>NON inheritable</b>
	*/
	class Connection
	{
	private:
		HENV _hEnv;			//!< Envirennement Handle 
		HDBC _hDbc;			//!< DB Connection Handle
		bool _bConnected;	//!< True if we are connected

		// Can't get a copy: 
		Connection(const Connection&);
		Connection & operator=(const Connection&);
	
	public:
		//! Default constructor
		/**
			It constructs a Connection object that is ready
			to connect.
		@see Connect()
		*/
		Connection();

		//! Construct and connect to a Data Source
		/**
			It constructs a Connection object and connects
			it to the desired Data Source.
		@param _dsn The name of the Data Source
		@param _user The username for authenticating to the Data Source.
			If _user is empty string, it will try to connect with the predefined
			user stored inside the Data Source.
		@param _pass The password for authenticating to the Data Source.
			If _pass is empty string, it will try to connect with the predefined
			password stored inside the Data Source.
		@remarks
			If the Connection fails, the object will be constructed properly
			but it will be unconnected. A failed Connection does not mean that
			the object is dirty, you can use Connect() to try connecting again.
		@see Connected(), Connect()
		*/
		Connection(const TString & _dsn,
			const TString & _user,
			const TString & _pass);

		//! Destructor
		/**
			It will Disconnect (if connected) from the db and
			all the Commands will be invalidated.
		*/
		~Connection();

		//! Connect to a Data Source
		/**
			Connect this object with a Data Source.
			If the object is already connected, it will
			disconnect automatically before trying to connect
			to the new Data Source.
		@param _dsn The name of the Data Source
		@param _user The username for authenticating to the Data Source.
			If _user is empty string, it will try to connect with the predefined
			user stored inside the Data Source.
		@param _pass The password for authenticating to the Data Source.
			If _pass is empty string, it will try to connect with the predefined
			password stored inside the Data Source.
		@return <b>True</b> if the Connection succeds or <b>False</b> if it fails.
			For extensive error reporting call LastError() or LastErrorStatusCode()
			after failure.		
		@see Disconnect(), Connect(), LastError(), LastErrorStatusCode()

        @ref example_1
		*/
		bool Connect(const TString & _dsn,
			const TString & _user,
			const TString & _pass);

		//! Check if it is connected
		/**
		@return <b>True</b> If the object is connected to any server, or
			<b>False</b> if it isn't.
        
        @ref example_1
		*/
		bool Connected() const;

		//! Close Connection
		/**
			If the object is connected it will close the Connection.
			If the object is already disconnected, calling Disconnect()
			will leave the object unaffected.

        @ref example_1
		*/
		void Disconnect();

		//! Get native HDBC handle
		/**
			This is the <b>DataBaseConnection</b>
			handle that the object has allocated
			internally with ODBC ISO API. This handle
			can be useful to anyone who needs to use ODBC ISO API
			along with SpODBC.
		@return Actual used ODBC database Connection handle for this instance.
		@see envHandle()
		*/
		HDBC dbcHandle()
		{
			return _hDbc;
		}

		//! Get native HENV handle
		/**
			This is the <b>Environment</b>
			handle that the object has allocated
			internally with ODBC ISO API. This handle
			can be useful to anyone who needs to use ODBC ISO API
			along with SpODBC.
		@return Actual used ODBC Environment handle for this instance.
		@see dbcHandle()
		*/
		HDBC envHandle()
		{
			return _hEnv;
		}

		//! Get last error description
		/**
			Get the description of the error that occurred
			with the last function call.
		@return If the last function call was successful it will
			return an empty string, otherwise it will return
			the description of the error that occurred inside
			the ODBC driver.
		@see LastErrorStatusCode()
		*/
		TString LastError();

		//! Get last error code
		/**
			Get the status of the error that occurred
			with the last function call.
		@return If the last function call was successful it will
			return an empty string, otherwise it will return
			the status code of the error that occurred inside
			the ODBC driver.
		@remarks The status codes are unique 5 length strings that
			correspond to a unique error. For information of
			this status code check ODBC API Reference (http://msdn.microsoft.com/en-us/library/ms716412(VS.85).aspx)
			
		@see LastError();
		*/
		TString LastErrorStatusCode();
	};	// !Connection

	//! Representation of result set field.
	/**
	@note odbc::Field is <B>Copyable</b>, <b>NON inheritable</b> and <b>NOT direct constructable</b>
	@remarks
		You <b>must</b> not create objects of this class directly but instead invoke Command::field() to get
		one. You <b>must not</b> keep the objects for more than directly calling one of its members, see Command::field()
		for more details on this.
	*/
	class Field
	{
	public:
		friend class Command;

	private:
		HSTMT _hStmt;			//!< Handle of Command that field exists
		int _colNum;			//!< Column number that field exists.
		
		// Not direct constructible
		Field(HSTMT _stmt, int __colNum);

	public:
	
		//! Copy constructor
		Field(const Field&);

		//! Copy operator
		Field & operator=(const Field&);

		//! Destructor
		~Field();

		//! @name Value retrieval functions
		//! @{

		//! Get field as string
		TString as_string() const;

		//! Get field as long
		long as_long() const;

		//! Get field as unsigned long
		unsigned long as_unsigned_long() const;

		//! Get field as short
		short as_short() const;

		//! Get field as unsigned short
		unsigned short as_unsigned_short() const;

		//! Get field as double
		double as_double() const;

		//! Get field as float
		float as_float() const;

		//! @}
	}; // !Field


	//! Handler prepared Commands parameters.
	/**
	@note SpODBC::Param is <B>Copyable</b>, <b>NON inheritable</b> and <b>NOT direct constructible</b>
	@remarks
		You <b>must</b> not create objects of this class directly but instead invoke Command::param() to get
		one. You <b>must not</b> keep the objects for more than directly calling one of its members, 
		see Command::param() for more details on this.
	*/
	class Param
	{
	public:
		friend class Command;

	private:
		HSTMT _hStmt;			//!< Handle of Command that parameter is set
		int _parNum;			//!< Order number of the parameter
		TString _int_string;	//!< Internal string buffer
		char _int_buffer[64];	//!< Internal buffer for small built-in types (64byte ... quite large)
		SQLINTEGER _int_SLOIP;	//!< Internal Str Length Or Indicator Pointer
		
		// Not direct constructible
		Param(HSTMT _stmt, int __parNum);

	public:
	
		//! Copy constructor
		Param(const Param&);

		//! Copy operator
		Param & operator=(const Param&);

		//! Destructor
		~Param();

		//! @name Value assignment functions
		//! @{

		//! Set parameter as string
		const TString & set_as_string(const TString & _str);
		
		//! Set parameter as long
		const long & set_as_long(const long & _value);

		//! Set parameter as unsigned long
		const unsigned long & set_as_unsigned_long(const unsigned long & _value);

		//! @}
	};	// !Param

	//! An ODBC Command representation object
	/**
		Represents a Command on the server. Statement is used to
		execute direct queries or prepare them and execute them
		multiple times with same or different parameters.
		
		A Command has a life-cycle from the time is opened
		to the time it is closed. A closed Command can be reused
		by reopening it.

		There is no need to directly open a Command, this is
		done automatically from the "Command construction"
		functions.
	@note SpODBC::Command is <B>Uncopiable</b> and <b>NON inheritable</b>
	*/
	class Command
	{
	private:
		HSTMT _hStmt;		//!< Command Handle
		bool _bOpen;		//!< True if Command has been opened

		// List of parameters
		typedef std::map<int, Param *> param_map_type;
		typedef param_map_type::iterator param_it;
		param_map_type m_params;

		// Uncopiable
		Command(const Command&);
		Command & operator=(const Command&);
	
	public:
		//! Default constructor
		/**
			It will construct a Command ready
			to execute or prepare a query. At the construction
			time Command is not opened, but it is ready to.
		*/
		Command();

		//! Construct and prepare
		/**
			It will construct and open a new Command at
			a desired Connection and prepare a query
			on it. After the construction the Command
			will be in open mode and will hold the prepared
			Command. You can use it to execute the Command
			multiple times, by passing (if any) different
			parameters.
		@param _conn The Connection object to prepare the query
			on it. Queries are always prepared on servers.
		@param _stmt The sql query to prepare.

		@remarks The prepared query is <b>not</b> stored on
			server but it is temporary and will get deleted when
			the Connection is closed or the Command.

			To check if the query was prepared successfully you
			can run is_open() after construction to see if it is
			opened. If the preparation fails, you can use this
			Command object to open other queries, direct or prepared.

		@note If you want to execute a direct query to the server without
			preparing it, you can use ExecuteDirect() after constructing
			a Command with the default constructor!
		@see prepare()
		*/
		Command(Connection & _conn, const TString & _stmt);

		//! Destructor
		/**
			It will close the query or delete the store prepared query,
			and close the result cursor if any.
		@see close();
		*/
		~Command();

		//! @name Core functionality
		//! @{

		//! Used to create a Command (used automatically by the "Command construction" functions)
		/**
			There is no need to call this function directly. It is called
			by "Statement construction" function when they want to open a new
			fresh Command with the server.

		@param _conn The Connection to the server where
			the Command will be opened inside it.

		@return
			- <b>True</b> If the Command was successfully opened in the server.
			- <b>False</b> If there was an error creating the Command.

		@remarks If there was an error opening a Command you can check
			for detailed error description with Connection::LastError()
			of the Connection object that you tried to open the Connection and
            <b>NOT</b> by calling Command::LastError() as a closed Command
			is unable to do error reporting.
		*/
		bool open(Connection & _conn);

		//! Check if it is an open Command
		bool is_open() const;

		//! Close Command
		/**
			It will close the Command, delete any stored results
			or prepared queries, or stored parameters.
		*/
		void close();

		//! Get native HSTMT handle
		/**
			This is the <b>Statement</b>
			handle that the object has allocated
			internally with ODBC ISO API. This handle
			can be useful to anyone who needs to use ODBC ISO API
			Along with SpODBC.
		@return Actual used ODBC Command handle for this instance.
		*/
		HDBC stmtHandle()
		{
			return _hStmt;
		}

		//! Get last error description
		/**
			Get the description of the error that occurred
			with the last function call.
		@return If the last function call was successful it will
			return an empty string, otherwise it will return
			the description of the error that occurred inside
			the ODBC driver.
		@see LastErrorStatusCode()
		*/
		TString LastError();

		//! Get last error code
		/**
			Get the status of the error that occurred
			with the last function call.
		@return If the last function call was successful it will
			return an empty string, otherwise it will return
			the status code of the error that occurred inside
			the ODBC driver.
		@remarks The status codes are unique 5 length strings that
			correspond to a unique error. For information of
			this status code check ODBC API Reference (http://msdn.microsoft.com/en-us/library/ms716412(VS.85).aspx)
			
		@see LastError();
		*/
		TString LastErrorStatusCode();

		//! @}

		//! @name Statement construction
		//! @{ 

		//! Prepare a query
		/**
			It will close any previous open operation and will prepare
			an sql query for execution. For more information about <b>query 
			preparation</b> visit http://msdn.microsoft.com/en-us/library/ms716365.aspx

			If you need to execute direct an sql query directly you can use
			ExecuteDirect().

		@param _conn The Connection object to prepare the query
			on it. Queries are always prepared on servers.
		@param _stmt The sql query to prepare.
		@return <b>True</b> if the preparation was successful or <b>False</b> if
			there was an error. In case of error check LastError() for detailed
			description of error.

		@note This is a <b>"Command construction" function</b> which means
			that any previous opened operation of this Command will be closed
			and a new Command will be created.
		@see param(), Execute(), FetchNext(), field(),  close()

        @ref example_4
		*/
		bool prepare(Connection & _conn, const TString & _stmt);

		//! Execute directly an sql query to the server.
		/**
			It will close any previous opened operation and will execute
			directly the desired sql query at the server.

		@param _conn The Connection object with the server where the
			query will be executed at.
		@param _query The sql query that will be execute at the server.
		@return <b>True</b> if the execution was successful or <b>False</b> if
			there was an error. In case of error check LastError() for detailed
			description of problem.
		@remarks
			After a successful execution of a query, the result cursor is pointed
			one slot before first row. To get the results of first row you 
			must first call once FetchNext() and then use field() 
			to get each field of the current row.

		@note This is a <b>"Command construction" function</b> which means
			that any previous opened operation of this Command will be closed
			and a new Command will be created.
		@see FetchNext(), count_columns(), field(), close()

        @ref example_2 \n
        @ref example_3
		*/
		bool ExecuteDirect(Connection & _conn, const TString & _query);

		//! @}

		//! @name Result gathering
		//! @{	

		//! Execute a prepared Command
		/**
			It will execute the query that was previously prepared
			in this Command. 
			
			To execute a prepared query there are some preconditions
			that must be satisfied.\n
			- The Command must be opened and an sql query must have
			been prepared.
			- Any previous result set must be have been freed.
			- All the parameters of the prepared Command must been passed
			with param()

		@return <b>True</b> if the prepared query was successfully executed
			or <b>False</b> if there was an error. In case of error 
			check LastError() for detailed	description of problem.

		@remarks
			After a successfully execution of a query, the result cursor is pointed
			one slot before first row. To get the results of first row you 
			must first call once FetchNext() and then use field() 
			to get each field of the current row.

		@see FetchNext(), count_columns(), field(), close()

        @ref example_4
		*/
		bool Execute();

		//! Fetch next result row
		/**
			If the Command has opened a result set, it
			will advance the cursor to the next row. Result sets
			are opened automatically when executing an sql query that 
			returns some results.

		@return
		- <b>True</b> if the cursor was advanced successfully.
		- <b>False</b> if the end of result set has been reached or
			there isn't opened any result set.

		@see field()

        @ref example_2
        @ref example_4
		*/
		bool FetchNext();

		//! Get a field of the current row in the result set
		/**
			It will return a field of the current selected
			row from the result set.
		@param _num The column of the field that you want
			to retrieve. First column is the 1.
		@return 
			- If the operation was successful a valid Field 
			object that contains info about the selected field.
			- An invalid Field if there isn't any
			result set opened or the result cursor is not pointing 
			to any valid row, or the number of the field was wrong.
		@remarks Objects returns from field() should be used directly and
			must not be stored.\n
			<b>good</b> example: @code m_long = field(1).as_long() @endcode\n
			<b>bad</b> example: @code Field tmp_field = field(1);  m_long = tmp_field.as_long(); @endcode
		@see FetchNext();

        @ref example_2 \n
        @ref example_4
		*/
		const Field field(int _num) const;

		//! Count columns of the result set
		/**
			It will return the number of columns of the
			current open result set.
		@return
			- If the operation was <b>successful</b> it will return a number
			<b>bigger than zero</b>.
			- <b>Less than zero or equal to</b> in case of any <b>error</b>.

        @ref example_2
		*/
		int count_columns() const;

		//! Free current opened result set.
		void free_results();

		//! @}

		//! @name Parameters handling
		//! @{

		//! Handle a parameter
		/**
			Returns a handle to a parameter of the prepared Command.
		@param _num The parameter number, starting from 1. This is the 
			way ODBC uses to identify the parameter markers. If there 
			are three parameters, the leftmost one is parameter no.1 
			and the rightmost is parameter no.3

		@return
			- If the operation was successful a valid Param
			object that can be used to set a value to the specific
			parameter.
			- An invalid Param object if there isn't any prepared
			Command, or the parameter number is wrong.
		@remarks Objects returns from param() should be used directly and
			must not be stored.\n
			<b>good</b> example: @code = param(1).set_as_long(1) @endcode\n
			<b>bad</b> example: @code Param tmp_parm = param(1);  tmp_parm.set_as_long(1); @endcode
		*/
		Param &param(int _num);

		//! Reset parameters (unbind all parameters)
		/**
			It will remove (unbind) all the assigned
			parameters of the prepared Command.

			In case that this is not a prepared Command
			it will fail silently.
		*/
		void reset_parameters();

		//! @}
	};	// !Command
};

#endif // !_SPODBC_H_DEFINED_

#endif // DB_ENGINE_ODBC