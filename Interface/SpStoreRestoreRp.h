#pragma once

//.=================================================================
//.
//.        Copyright (c) Matra Transport International 2000
//.        Ce fichier est juridiquement prot�g� par la loi 98-531  
//.        du 1er juillet 1998 
//.        La version et les �volutions sont identifi�es dans le  
//.        fichier de version associ� .ver
//.
//.        File : SpStoreRestoreRp.h
//.        ----
//.
//.        Content : >> describe the purpose of the file <<
//.        -------
//.
//.=================================================================

#include <SpStore/Interface/SpDBEngine.h>
#include <SpStore/Interface/SpStoreDBMgr.h>
#include <SpStore/Interface/SpStoreRp.h>
#include <ilserver/model.h>
class SpStoreRestoreOwnerRp;

static class SpStoreRestoreDummy_ToForceLink {public: SpStoreRestoreDummy_ToForceLink();} SpStoreRestoreDummy_ToForceLink_;


//.=================================================================
//.Class : SpStoreRestoreRp
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 02/11/00 Creation : Alexis Kolybine
//. 
//.Inheritance Path : 
//.---------------- 
//. SpReconRpObject 
//. -> SpStoreRestoreRp
//. 
//.Description : >> describe the purpose of the class <<
//.-----------   
//. 
//.Limitation : none
//.---------- 
//. 
//.=================================================================
class SpStoreRestoreRp : public IlsRpObject/*SpReconRpObject*/
{
public :
	SpStoreRestoreRp (IlsRepresentation& rp, const IlsRpObjModel& m)
	   : /*SpReconRpObject*/IlsRpObject(rp, m) {;}

	SpStoreRestoreRp(const IlsRpObject& collectorOwner,
              IlsRpAttributeId collectorAttrId,
              const IlsRpObjModel* model=0,
              IlsBoolean endUpdate=IlsTrue)
		: IlsRpObject(collectorOwner, collectorAttrId, model, endUpdate)
	{} // Constructeurs pour les objets cr��s dynamiquement.

	const IlsString& getId() const { return _id; }

protected : 
	// Methode appell�e sur creation du Rp (associ�e � une macro ILOG)
	// => reference chaque nouveau Rp dans la map '_rp_to_restore'
	void setId(const IlsString &id);

	IlsString _id;

private : 
	ILS_RP_OBJECT_DECL(SpStoreRestoreRp)
};


//.=================================================================
//.Class : SpStoreRestoreOwnerRp
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 06/01/04 Creation : ENA
//. 
//.Inheritance Path : 
//.---------------- 
//. IlsRpObject 
//. -> SpStoreRestoreOwnerRp
//. 
//.Description : Rp des objets standards (cr��s par CSV) auxquels
//.-----------   sont associ�s des objets dynamiques.
//. 
//.Limitation : S'applique aux objets serveurs "standard" (issus de CSV)
//.----------   mais etant le conteneur d'un objet 'SpStoreRestoreDynRp'.
//. 
//.=================================================================
class SpStoreRestoreOwnerRp : public IlsRpObject/*SpReconRpObject*/
{
public :
	SpStoreRestoreOwnerRp (IlsRepresentation& rp, const IlsRpObjModel& m)
	   : IlsRpObject(rp, m) {;}

	SpStoreRestoreOwnerRp(const IlsRpObject& collectorOwner,
              IlsRpAttributeId collectorAttrId,
              const IlsRpObjModel* model=0,
              IlsBoolean endUpdate=IlsTrue)
	   : IlsRpObject(collectorOwner, collectorAttrId, model, endUpdate)
	{;}

	const IlsString& getId() const { return _id; }

protected :
	// Methode appell�e sur creation du Rp (associ�e � une macro ILOG)
	virtual void setId(const IlsString &id);

	IlsString _id;

private : 
	ILS_RP_OBJECT_DECL(SpStoreRestoreOwnerRp)
};


//.=================================================================
//.Class : SpStoreRestoreDynRp
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 06/01/04 Creation : ENA
//. 
//.Inheritance Path : 
//.---------------- 
//. IlsRpObject 
//. -> SpStoreRestoreOwnerRp
//.    -> SpStoreRestoreDynRp
//. 
//.Description : Rp associ�s � des objets dynamiques qui ne sont pas
//.-----------   cr��s par les mecanismes standard (CSV).
//.              Le Restore cr��s les Rp (et donc les objets serveurs
//.              associ�s) ainsi que les relations entre objets.
//.
//.Limitation : Les objets dynamiques doivent avoir un identifiant
//.----------   unique pour etre r�f�renc� correctement dans la
//.             table de 'store' (corr�lation store/restore).
//.
//.             Le constructeur des objets serveur derivant de 
//.             la classe 'SpSrvObject' ne permet pas de construire
//.             des objets dynamiques a partir d'un Rp.
//.             => les objets serveurs d�rivent de 'IlsObject'.
//. 
//.=================================================================
class SpStoreRestoreDynRp : public SpStoreRestoreOwnerRp
{
public :
	SpStoreRestoreDynRp (IlsRepresentation& rp, const IlsRpObjModel& m)
		: SpStoreRestoreOwnerRp(rp, m)
	{;}

	SpStoreRestoreDynRp(const IlsRpObject& collectorOwner,
              IlsRpAttributeId collectorAttrId,
              const IlsRpObjModel* model=0,
              IlsBoolean endUpdate=IlsTrue)
		: SpStoreRestoreOwnerRp(collectorOwner, collectorAttrId, model, endUpdate)
	{;}

	// Methode de mise a jour de l'id du Rp lors de sa creation.
	void identify(const IlsString& id);

protected : 

private : 
	ILS_RP_OBJECT_DECL(SpStoreRestoreDynRp)
};


//.=================================================================
//.Class : SpStoreRestoreRepresentation
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 03/11/00 Creation : Alexis Kolybine
//. 
//.Inheritance Path : 
//.---------------- 
//. SpReconRepresentation 
//. -> SpStoreRestoreRepresentation
//. 
//.Description : >> describe the purpose of the class <<
//.----------- 
//. 
//.Limitation : >> for example singleton class, dependency ... <<
//.---------- 
//. 
//.=================================================================


class SpStoreRestoreRepresentation : public SpStoreRepresentation
{
 public:
  SpStoreRestoreRepresentation(IlsMvComponent& c, const IlsRpModel& m)
    : SpStoreRepresentation(c,m)
    {
    }

  virtual ~SpStoreRestoreRepresentation();

  // Methode de fin de transaction sur l'objet de representation.
  // Si il s'agit d'une notication de creation (ouverture de la vue)
  // alors l'operation de restore est effectu�e.
  void endS2CTransaction(IlsS2CTransStatus transStatus,
                               IlsTransactionId);

  //=====================================================
  // Relations entre id des Rp et les Rp.

  void addRpToRestore(SpStoreRestoreRp* rp)
  { _rp_to_restore[rp->getId().value()] = rp;	}

  void addOwnerRp(SpStoreRestoreOwnerRp* rp)
  { _ownerRpMap[rp->getId().value()] = rp;	}

  //=====================================================
  // Relations entre id de l'enregistrement
  // et les propri�t�s associ�es.

  void addDynRpToRestore(const std::string& id, SpStoreRestoreDynRp* rp);

  void addOwnerRelation(const std::string& objectId, const std::string& ownerId)
  { _ownerRelationMap[objectId] = ownerId;	}
  
  void addSvTypeName(const std::string& objectId, const std::string& svTypeName) // Peut etre du type SpStoreRestoreDynRp ou SpStoreRestoreOwnerRp
  { _svTypeNameMap[objectId] = svTypeName;	}
  
  void addCollectorId(const std::string& objectId, const std::string& svTypeName) // Peut etre du type SpStoreRestoreDynRp ou SpStoreRestoreOwnerRp
  { _collectorIdMap[objectId] = svTypeName;	}

  //=====================================================
  
 protected:
 	int  restore();					// Traitement du Restore conventionnel

 	int  restoreDyn();				// Traitement du Restore des objets cr��s/supprim�s dynamiquement
 	int  initDynCollector();		// -> Identifie les elements associ�s a un collecteur
 	int  initDynSvTypeName();		// -> Identifie les elements associ�s a une classe de serveur (cration dynamique de l'objet)
	int  initDynOwnerRelation();	// -> Identifie les relations d'objet contenant/contenu
	int  restoreDynObject();		// -> Restaure les objets dynamiques
	int  restoreDynRelation();		// -> Restaure les relations inverses dans le cas o� le Owner est un objet dynamique.

	SpStoreRestoreDynRp* createDynRp(const IlsString& rowId);					// Cr�e un objet dynamique, ainsi que la relation inverse avec un objet non dynamique (SpStoreRestoreOwnerRp).
	void updateRpValue(IlsRpObject* rp, SpDBEngine::t_RestoreRow& row);	// Mise a jour pour l'objet dynamique d'un de ces attributs.
 private:
	 std::map<std::string, SpStoreRestoreRp*> _rp_to_restore;         // Liste des Rp a mettre a jour par le Restore
	 std::map<std::string, SpStoreRestoreDynRp*> _dynRp_to_restore;   // Liste des Rp cr��s dynamiquement par le Restore
	 std::map<std::string, SpStoreRestoreOwnerRp*> _ownerRpMap;       // Liste des Rp de type SpStoreRestoreOwnerRp (objets statiques)
	 std::map<std::string, std::string> _ownerRelationMap;            // Relation entre objets Rp definis dans la base (objets dynamiques uniquement)
	 std::map<std::string, std::string> _svTypeNameMap;               // Relation Rp / classe serveur associ�e
	 std::map<std::string, std::string> _collectorIdMap;              // Relation Rp / nom du collecteur dans la relation avec le conteneur.

	 ILS_REPRESENTATION_DECL(SpStoreRestoreRepresentation)
};



