#pragma once
//*************************************************************************************************
//
//  Copyright (c) Siemens Transportation Systems 2005
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version associ� (.ver).
//
//  Fichier  : SpStoreTraces.h
//  Auteur   : Daniel RAFFIN
//  Cr�ation : mercredi 2 f�vrier 2005
//  Contenu  : D�claration des macros de traces
//
//*************************************************************************************************

#include <XtWin/Interface/XtWinLogger.h>

// D�sactivation des traces si SPSTORE_LOG_DEBUG n'est pas d�finie
#ifndef SPSTORE_LOG_DEBUG
#  define SPSTORE_NDEBUG
#endif

// traces 'spocc.store'
#ifndef SPSTORE_NDEBUG
#  define SPSTORE_DEBUG(msg)			XTWIN_LOGDEBUG(spocc.store, msg)
#else
#  define SPSTORE_DEBUG(msg)
#endif
#define SPSTORE_TRACE(msg)				XTWIN_LOGTRACE(spocc.store, msg)
#define SPSTORE_INFO(msg)				XTWIN_LOGINFO(spocc.store, msg)
#define SPSTORE_NOTE(msg)				XTWIN_LOGNOTE(spocc.store, msg)
#define SPSTORE_WARNING(msg)			XTWIN_LOGWARNING(spocc.store, msg)
#define SPSTORE_ERROR(msg)				XTWIN_LOGERROR(spocc.store, msg)
#define SPSTORE_FATAL_ERROR(msg)		XTWIN_LOGFATALERROR(spocc.store, msg)
#define SPSTORE_DUMP(msg, data, size)	XTWIN_LOGDUMP(spocc.store, msg, data, size)

// traces conditionnelles 'spocc.store'
#define SPSTORE_DEBUGIF(pre, msg)		XTWIN_LOGDEBUGIF(spocc.store, pre, msg)
#define SPSTORE_TRACEIF(pre, msg)		XTWIN_LOGTRACEIF(spocc.store, pre, msg)
#define SPSTORE_INFOIF(pre, msg)		XTWIN_LOGINFOIF(spocc.store, pre, msg)
#define SPSTORE_NOTEIF(pre, msg)		XTWIN_LOGNOTEIF(spocc.store, pre, msg)
#define SPSTORE_WARNINGIF(pre, msg)		XTWIN_LOGWARNINGIF(spocc.store, pre, msg)
#define SPSTORE_ERRORIF(pre, msg)		XTWIN_LOGERRORIF(spocc.store, pre, msg)
#define SPSTORE_FATALERRORIF(pre, msg)	XTWIN_LOGFATALERRORIF(spocc.store, pre, msg)
