# Microsoft Developer Studio Project File - Name="SpRestore" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=SpRestore - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SpRestore.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SpRestore.mak" CFG="SpRestore - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SpRestore - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "SpRestore - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "SpRestore"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SpRestore - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
LINK32=link.exe
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /G5 /MT /W3 /GR /GX /Zi /Ot /Oi /Ob1 /I "$(GCL_XT_CPP)" /I "$(GCL_SPOCC)" /I "$(ILSHOME)\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_LIB" /D "_MT" /D "_MBCS" /D "DBNTWIN32" /D "IL_STD" /D "ILMSVCSTD" /D "XTWIN_NLOGDEBUG" /FD /c
# SUBTRACT CPP /Og /YX
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo
# Begin Special Build Tool
TargetPath=.\Release\SpRestore.lib
TargetName=SpRestore
SOURCE="$(InputPath)"
PostBuild_Cmds=IF NOT EXIST "$(GCL_SPOCC)\SPOCC\lib" MKDIR "$(GCL_SPOCC)\SPOCC\lib"	COPY "$(TargetPath)" "$(GCL_SPOCC)\SPOCC\lib\$(TargetName).lib"
# End Special Build Tool

!ELSEIF  "$(CFG)" == "SpRestore - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
LINK32=link.exe
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /G5 /MT /W3 /GR /GX /Zi /Od /I "$(GCL_XT_CPP)" /I "$(GCL_SPOCC)" /I "$(ILSHOME)\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_LIB" /D "_MT" /D "_MBCS" /D "DBNTWIN32" /D "IL_STD" /D "ILMSVCSTD" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo
# Begin Special Build Tool
TargetPath=.\Debug\SpRestore.lib
TargetName=SpRestore
SOURCE="$(InputPath)"
PostBuild_Cmds=IF NOT EXIST "$(GCL_SPOCC)\SPOCC\lib" MKDIR "$(GCL_SPOCC)\SPOCC\lib"	COPY "$(TargetPath)" "$(GCL_SPOCC)\SPOCC\lib\$(TargetName).lib"
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "SpRestore - Win32 Release"
# Name "SpRestore - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\Code\SpStoreRestoreRp.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Interface\SpStoreRestoreRp.h
# End Source File
# End Group
# End Target
# End Project
